<?php

/**
 * Implementation of hook_views_plugins
 */
function view_collection_views_plugins() {
  $path = drupal_get_path('module', 'view_collection');
  $views_path = drupal_get_path('module', 'views');
  require_once "./$path/theme/theme.inc";
  return array(
    'module' => 'view_collection',
    'style' => array(
      'view_collection_style' => array(
        'title' => t('CTICFF Collection View'),
        'help' => t('Displays nodes.'),
        'handler' => 'view_collection_plugin_style',
        'path' => "$path/views",
        'theme' => 'view_collection_view_style',
        'theme file' => 'theme.inc',
        'theme path' => "$path/theme",
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
    'row' => array(
      'view_collection_row' => array(
        'title' => t('CTICFF Collection Row'),
        'help' => t('(Displays a CTICFF Format Row'),
        'handler' => 'view_collection_plugin_row',
        'path' => "$path/views",
        'theme' => 'view_collection_view_row',
        'theme file' => 'theme.inc',
        'theme path' => "$path/theme",
        'uses fields' => TRUE,
        'type' => 'normal',
      ),
    )
  );
}

<?php
/**
 * @file
 * Contains the default style plugin.
 */

/**
 * Default style plugin to render rows one after another with no
 * decorations.
 *
 * @ingroup views_style_plugins
 */
class view_collection_plugin_style extends views_plugin_style {
  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['type'] = array('default' => 'ul');

    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['type'] = array(
      '#type' => 'radios',
      '#title' => t('List type'),
      '#options' => array('ul' => t('Unordered list'), 'ol' => t('Ordered list')),
      '#default_value' => $this->options['type'],
    );
    $form['is_filter_year'] = array(
      '#type' => 'radios',
      '#title' => t('Show Filter Year'),
      '#options' => array('1' => t('Yes'), '0' => t('No')),
      '#default_value' => $this->options['is_filter_year'],
    );
    $form['filter_year'] = array(
      '#type' => 'textfield',
      '#title' => t('Filter Year'),
      '#default_value' => $this->options['filter_year'],
    );
    $form['is_filter_title'] = array(
      '#type' => 'radios',
      '#title' => t('Show Filter Title'),
      '#options' => array('1' => t('Yes'), '0' => t('No')),
      '#default_value' => $this->options['is_filter_title'],
    );
    $form['filter_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Filter Title'),
      '#default_value' => $this->options['filter_title'],
    );
  }
}

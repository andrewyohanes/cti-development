<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->separator: an optional separator that may appear before a field.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<article class="thumb-card">
<?php foreach ($fields as $id => $field): ?>
  <?php
    $content = $field->content;
    if($field->class == "field-video-value") {
      $url = $field->content;
      parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
      $vidID = $my_array_of_vars['v'];
      $content = "<a class='myvid' href='$url'>
                  <img alt='' src='https://img.youtube.com/vi/$vidID/0.jpg'>
                </a>";
    }
    
  ?>
  
  <<?php print $field->inline_html;?> class="views-field-<?php print $field->class; ?>">
    <?php if ($field->label): ?>
      <label class="views-label-<?php print $field->class; ?>">
        <?php print $field->label; ?>:
      </label>
    <?php endif; ?>
      <<?php print $field->element_type; ?> class="field-content"><?php print $content; ?></<?php print $field->element_type; ?>>
  </<?php print $field->inline_html;?>>
<?php endforeach; ?>
</article>
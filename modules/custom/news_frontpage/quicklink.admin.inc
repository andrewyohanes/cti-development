<?php 
// $Id$
/**
 * @file
 * Administration page callbacks for the quicklink module.
 */

/**
 * Form builder.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function quicklink_admin_settings() {
  $form['quicklink'] = array(
    '#type' => 'fieldset',
    '#title' => t('Quicklink'),
  );
  
  $form['quicklink']['type_1'] = array(
    '#type' => 'select', 
    '#title' => t('Type link 1st'), 
    '#options' => array(
      'none' => t('None'), 
      'node' => t('Node link'), 
      'outside' => t('Outside link')
    ),
    '#default_value' => variable_get('quicklink_type_1', "")
  );
  $form['quicklink']['node_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Node'),
    '#suffix' => '<a href="#" class="add-new-node views-ajax-link views-processed hilite" id="add-new-node-1">+ Add Link</a>',
    '#default_value' => variable_get('quicklink_node_1', "")
  );
  $form['quicklink']['node_1_id'] = array(
    '#type' => 'hidden',
    '#value' => '',
    '#default_value' => variable_get('quicklink_nodeid_1', "")
  );
  $form['quicklink']['text_out_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Text'),
    '#default_value' => variable_get('quicklink_text_out_1', "")
  );
  $form['quicklink']['link_out_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Link'),
    '#default_value' => variable_get('quicklink_link_out_1', "")
  );
  
  
  $form['quicklink']['type_2'] = array(
    '#type' => 'select', 
    '#title' => t('Type link 2nd'), 
    '#options' => array(
      'none' => t('None'), 
      'node' => t('Node link'), 
      'outside' => t('Outside link')
    ),
    '#default_value' => variable_get('quicklink_type_2', "")
  );
  $form['quicklink']['node_2'] = array(
    '#type' => 'textfield',
    '#title' => t('Node'),
    '#suffix' => '<a href="#" class="add-new-node views-ajax-link views-processed hilite" id="add-new-node-2">+ Add Link</a>',
    '#default_value' => variable_get('quicklink_node_2', "")
  );
  $form['quicklink']['node_2_id'] = array(
    '#type' => 'hidden',
    '#value' => '',
    '#default_value' => variable_get('quicklink_nodeid_2', "")
  );
  $form['quicklink']['text_out_2'] = array(
    '#type' => 'textfield',
    '#title' => t('Text'),
    '#default_value' => variable_get('quicklink_text_out_2', "")
  );
  $form['quicklink']['link_out_2'] = array(
    '#type' => 'textfield',
    '#title' => t('Link'),
    '#default_value' => variable_get('quicklink_link_out_2', "")
  );
  
  
  $form['quicklink']['type_3'] = array(
    '#type' => 'select', 
    '#title' => t('Type link 3rd'), 
    '#options' => array(
      'none' => t('None'), 
      'node' => t('Node link'), 
      'outside' => t('Outside link')
    ),
    '#default_value' => variable_get('quicklink_type_3', "")
  );
  $form['quicklink']['node_3'] = array(
    '#type' => 'textfield',
    '#title' => t('Node'),
    '#suffix' => '<a href="#" class="add-new-node views-ajax-link views-processed hilite" id="add-new-node-3">+ Add Link</a>',
    '#default_value' => variable_get('quicklink_node_3', "")
  );
  $form['quicklink']['node_3_id'] = array(
    '#type' => 'hidden',
    '#value' => '',
    '#default_value' => variable_get('quicklink_nodeid_3', "")
  );
  $form['quicklink']['text_out_3'] = array(
    '#type' => 'textfield',
    '#title' => t('Text'),
    '#default_value' => variable_get('quicklink_text_out_3', "")
  );
  $form['quicklink']['link_out_3'] = array(
    '#type' => 'textfield',
    '#title' => t('Link'),
    '#default_value' => variable_get('quicklink_link_out_3', "")
  );
  
  
  $form['quicklink']['type_4'] = array(
    '#type' => 'select', 
    '#title' => t('Type link 4th'), 
    '#options' => array(
      'none' => t('None'), 
      'node' => t('Node link'), 
      'outside' => t('Outside link')
    ),
    '#default_value' => variable_get('quicklink_type_4', "")
  );
  $form['quicklink']['node_4'] = array(
    '#type' => 'textfield',
    '#title' => t('Node'),
    '#suffix' => '<a href="#" class="add-new-node views-ajax-link views-processed hilite" id="add-new-node-4">+ Add Link</a>',
    '#default_value' => variable_get('quicklink_node_4', "")
  );
  $form['quicklink']['node_4_id'] = array(
    '#type' => 'hidden',
    '#value' => 'quicklink_nodeid_4',
    '#default_value' => variable_get('quicklink_nodeid_4', "")
  );
  $form['quicklink']['text_out_4'] = array(
    '#type' => 'textfield',
    '#title' => t('Text'),
    '#default_value' => variable_get('quicklink_text_out_4', "")
  );
  $form['quicklink']['link_out_4'] = array(
    '#type' => 'textfield',
    '#title' => t('Link'),
    '#default_value' => variable_get('quicklink_link_out_4', "")
  );
  
  
  return system_settings_form($form);
}

/**
 * Validate configuration form.
 */
function quicklink_admin_settings_validate($form, $form_state) {
  variable_set('quicklink_type_1', $form_state["clicked_button"]["#post"]['type_1']);
  variable_set('quicklink_node_1', $form_state["clicked_button"]["#post"]['node_1']);
  variable_set('quicklink_nodeid_1', $form_state["clicked_button"]["#post"]['node_1_id']);
  variable_set('quicklink_text_out_1', $form_state["clicked_button"]["#post"]['text_out_1']);
  variable_set('quicklink_link_out_1', $form_state["clicked_button"]["#post"]['link_out_1']);
  
  variable_set('quicklink_type_2', $form_state["clicked_button"]["#post"]['type_2']);
  variable_set('quicklink_node_2', $form_state["clicked_button"]["#post"]['node_2']);
  variable_set('quicklink_nodeid_2', $form_state["clicked_button"]["#post"]['node_2_id']);
  variable_set('quicklink_text_out_2', $form_state["clicked_button"]["#post"]['text_out_2']);
  variable_set('quicklink_link_out_2', $form_state["clicked_button"]["#post"]['link_out_2']);
  
  variable_set('quicklink_type_3', $form_state["clicked_button"]["#post"]['type_3']);
  variable_set('quicklink_node_3', $form_state["clicked_button"]["#post"]['node_3']);
  variable_set('quicklink_nodeid_3', $form_state["clicked_button"]["#post"]['node_3_id']);
  variable_set('quicklink_text_out_3', $form_state["clicked_button"]["#post"]['text_out_3']);
  variable_set('quicklink_link_out_3', $form_state["clicked_button"]["#post"]['link_out_3']);
  
  variable_set('quicklink_type_4', $form_state["clicked_button"]["#post"]['type_4']);
  variable_set('quicklink_node_4', $form_state["clicked_button"]["#post"]['node_4']);
  variable_set('quicklink_nodeid_4', $form_state["clicked_button"]["#post"]['node_4_id']);
  variable_set('quicklink_text_out_4', $form_state["clicked_button"]["#post"]['text_out_4']);
  variable_set('quicklink_link_out_4', $form_state["clicked_button"]["#post"]['link_out_4']);
}




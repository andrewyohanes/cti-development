<?php
/**
 * @file views-view-list.tpl.php
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<?php
  $rootheme = check_url($front_page) .'sites/all/themes/new_ctiff';
?>
<?php 
  global $base_url;
  global $base_path;
  
  $breadcrumb = array();
  $breadcrumb[] = l('Home', '');
  $breadcrumb[] = l(drupal_get_title(), $base_url . base_path() . $view->display_handler->options['path']);

  // Set Breadcrumbs
  $breadcrumbs = drupal_set_breadcrumb($breadcrumb);
  
  $fullpath = $base_url . $base_path . $_GET['q'];

  if($request_param != null){
    $current = $_GET['q'];
    $param = '/' . implode('/', $request_param);
    $path = str_replace($param, "", $current);
    $fullpath = $base_url . $base_path . $path;
  }
?>
<section class='thumbnail-news'>
  <div class='entry-header'>
    <div class='content'>
      <div class="breadcrumb">
        <ol class="breadcrumb">
          <?php foreach($breadcrumbs as $breadcrumb): ?>
            <li><?php echo $breadcrumb; ?></li>
          <?php endforeach; ?>
        </ol>
      </div>
      <?php if (!empty($title)) : ?>
        <h1><?php print $title; ?></h1>
      <?php else: ?>
        <h1>News Release</h1>
      <?php endif; ?>
      <div class='filter-content'>
        <form class='form-inline' id="form-filter">
          <div class='form-group'>
            <label>
              VIEWS :
            </label>
          </div>
          <div class="form-group">
            <select name='<?php print $options['field_year']; ?>' id='year'>
              <option value=''>All Years</option>
              <?php for($i=date('Y'); $i>=$options['filter_year']; $i--):?>
                <option value='<?php echo $i; ?>' <?php if($i == $request_param['field_document_date_value']): ?>selected="selected"<?php endif; ?> ><?php echo $i; ?></option>
              <?php endfor;?>
            </select>
          </div>
          <div class='form-group'>
            <input name='submit' type='submit' id='SubmitButton' value='GO'>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class='entry-content'>
    <<?php print $options['type']; ?> class="grid-thumb">
      <?php foreach ($rows as $id => $row): ?>
        <li class="<?php print $classes[$id]; ?> grid-item"><?php print $row; ?></li>
      <?php endforeach; ?>
    </<?php print $options['type']; ?>>
  </div>
</section>

<script type="text/javascript">
  $(document).ready(function(){
    target = $('#form-filter').closest('.view')[0];
    
    $('.pg_link').each(function(){
      $(this).on('click',function(){
        breadcrumb = $('div.breadcrumb').html();
        var yearName = $('#year').attr("name");
        yearValue = $('#year').val();
        view_form = $('.page-content').clone();
        view_form.find('.entry-content').remove();
        var page = $(this).attr('data-page');
        
        var post = {
          "args": <?php echo json_encode($query_args); ?>,
          "filters": [{ "name": yearName, "value":yearValue }],
          "show_per_page": "<?php echo $items_per_page; ?>",
          "query": "<?php echo trim(preg_replace('/\s\s+/', ' ', $query)); ?>",
          "view_name": "<?php echo $view_name; ?>",
          "view_form": view_form.html(),
        };
        //console.log(post);
        if($('.page-content .item-list').length > 0) {
          $('.page-content .item-list').remove();
        }
      
        $.post("<?php echo $base_url; ?>/api/v1/news/list/" + page, post, function(response, textStatus,jqXHR){
          $(this).removeClass('views-throbbing');
          // Scroll to the top of the view. This will allow users
          // to browse newly loaded content after e.g. clicking a pager
          // link.
          var offset = $(target).offset();
          // We can't guarantee that the scrollable object should be
          // the body, as the view could be embedded in something
          // more complex such as a modal popup. Recurse up the DOM
          // and scroll the first element that has a non-zero top.
          var scrollTarget = target;
          while ($(scrollTarget).scrollTop() == 0 && $(scrollTarget).parent()) {
            scrollTarget = $(scrollTarget).parent()
          }
          // Only scroll upward
          if (offset.top - 10 < $(scrollTarget).scrollTop()) {
            $(scrollTarget).animate({scrollTop: (offset.top - 10)}, 500);
          }
          // Call all callbacks.
          if (response.__callbacks) {
            $.each(response.__callbacks, function(i, callback) {
              eval(callback)(target, response);
            });
          }
        
          $('#year').val(yearValue);
          $('div.breadcrumb').html(breadcrumb);
          new window.CoralCTI.Frontend({});
        }, 'json');
        
        return false;
      });
    });
    
    replaceImage2 = function(){
      $(".views-field-field-image-fid").each(function() {
        // if image already loaded, we can check it's height now
        if ($(this).find('.field-content').html()=="") {
          $newIMG = $('<img>');
          $newIMG.attr("src","<?php print $rootheme; ?>/images/section/default-picture.jpg");
          $newIMG.attr("width","372");
          $newIMG.attr("height","238");
          $(this).find('.field-content').append($newIMG);
        }
      });
    };
    replaceImage2();
    
    function checkImg(img) {
      if (img.naturalHeight <= 1 && img.naturalWidth <= 1) {
        // undersize image here
        img.src = "<?php print $rootheme; ?>/images/section/default-picture.jpg";
      }
    }
    replaceImage = function(){
      $("img").each(function() {
        // if image already loaded, we can check it's height now
        if (this.complete) {
          checkImg(this);
        } else {
          // if not loaded yet, then set load and error handlers
          $(this).load(function() {
              checkImg(this);
          }).error(function() {
              // img did not load correctly
              // set new .src here
              this.src = "<?php print $rootheme; ?>/images/section/default-picture.jpg";
          });
        }
      });
    };
    replaceImage();
    
    $('#form-filter').submit(function(){

      var yearName = $('#year').attr("name");
      yearValue = $('#year').val();
      breadcrumb = $('div.breadcrumb').html();
      view_form = $('.page-content').clone();
      view_form.find('.entry-content').remove();
      
      var post = {
        "args": <?php echo json_encode($query_args); ?>,
        "filters": [{ "name": yearName, "value":yearValue }],
        "show_per_page": "<?php echo $items_per_page; ?>",
        "query": "<?php echo trim(preg_replace('/\s\s+/', ' ', $query)); ?>",
        "view_name": "<?php echo $view_name; ?>",
        "view_form": view_form.html(),
      };
      //console.log(post);
      if($('.page-content .item-list').length > 0) {
        $('.page-content .item-list').remove();
      }
      $.post("<?php echo $base_url; ?>/api/v1/news/list/1", post, function(response, textStatus,jqXHR){
        $(this).removeClass('views-throbbing');
        // Scroll to the top of the view. This will allow users
        // to browse newly loaded content after e.g. clicking a pager
        // link.
        var offset = $(target).offset();
        // We can't guarantee that the scrollable object should be
        // the body, as the view could be embedded in something
        // more complex such as a modal popup. Recurse up the DOM
        // and scroll the first element that has a non-zero top.
        var scrollTarget = target;
        while ($(scrollTarget).scrollTop() == 0 && $(scrollTarget).parent()) {
          scrollTarget = $(scrollTarget).parent()
        }
        // Only scroll upward
        if (offset.top - 10 < $(scrollTarget).scrollTop()) {
          $(scrollTarget).animate({scrollTop: (offset.top - 10)}, 500);
        }
        // Call all callbacks.
        if (response.__callbacks) {
          $.each(response.__callbacks, function(i, callback) {
            eval(callback)(target, response);
          });
        }
        //console.log(response);
        $('#year').val(yearValue);
        $('div.breadcrumb').html(breadcrumb);
        new window.CoralCTI.Frontend({});
      }, 'json');
      
      return false;
    });

    // document.getElementById('year').value=2017;
    // var btnSubmitTags = document.getElementById('SubmitButton');
    // btnSubmitTags.click();
  });
  window.onload = function(){
    document.getElementById('year').value=2017;
    var btnSubmitTags = document.getElementById('SubmitButton');
    btnSubmitTags.click();
  };
</script>



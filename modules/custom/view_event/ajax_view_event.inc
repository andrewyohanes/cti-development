<?php 
// $Id$
/**
 * @file
 */

function collection_list() {
  $args = $_POST['args'];
  $filters = $_POST['filters'];
  $show_per_page = $_POST['show_per_page'];
  $sql = $_POST['query'];
  $view_toload = $_POST['view_name'];
  $form = $_POST['view_form'];

  $result = array();
  
  if(!empty($filters)) {
    $sql .= " AND ";
    $i = 0;
    foreach($filters as $filter){
      if(!empty($filter['type']) && $filter['type'] == 'range') {
        if($i > 0){
          $sql .= " AND ";
        }
        $sql .= $filter['name'] . " BETWEEN '" . $filter['value1'] . "'  AND '" . $filter['value2'] ."'" ;
      } else {
        if(!empty($filter['value'])){
          if($i > 0){
            $sql .= " AND ";
          }
          $sql .= $filter['name'] . " = '" . $filter['value'] . "'" ;
        }
      }
      $i++;
    }
  }

  $output = let_view_show($view_toload, $sql, $form);
  
  header('Content-Type: application/json');
  $arrayOutput = json_decode(json_encode($output), True);
  echo json_encode($arrayOutput);
  exit;
}

function let_view_show($name, $sql, $form){
  $display_id = 'page_1';
  if ($view = collection_get_view($name, $sql)) {
    if ($view->access($display_id)) {

      // Fix 'q' for paging.
      if (!empty($path)) {
        $_GET['q'] = $path;
      }

      $view->dom_id = $dom_id;
      $errors = $view->validate();
      
      if ($errors === TRUE) {
        if(!empty($view->result)) {
          $object->status = TRUE;
          $object->display .= $view->preview($display_id, $args);
          $object->title = $view->get_title();
        }else {
          $object->status = TRUE;
          $object->display .= $form;
          $object->title = $view->get_title();
        }
      }
      else {
        foreach ($errors as $error) {
          drupal_set_message($error, 'error');
        }
      }
      // Register the standard JavaScript callback.
      $object->__callbacks = array('Drupal.Views.Ajax.ajaxViewResponse');
      // Allow other modules to extend the data returned.
      drupal_alter('ajax_data', $object, 'views', $view);
    }
  }
  $messages = theme('status_messages');
  $object->messages = $messages ? '<div class="views-messages">' . $messages . '</div>' : '';

  return $object;
}

function collection_get_view($name, $sql) {
  require_once drupal_get_path('module', 'view_event') . "/view_event.class.inc";

  $view = view_event::load($name);
  $default_view = views_get_default_view($name);

   // The view does not exist.
  if (empty($view) && empty($default_view)) {
    return;
  }
  // The view is defined in code.
  elseif (empty($view) && !empty($default_view)) {
    $status = variable_get('views_defaults', array());
    if (isset($status[$default_view->name])) {
      $default_view->disabled = $status[$default_view->name];
    }
    $default_view->type = t('Default');
    return $default_view->clone_view();
  }
  // The view is overriden/defined in the database.
  elseif (!empty($view) && !empty($default_view)) {
    $view->type = t('Overridden');
  }
  $view = $view->update_query_result($sql);

  return $view;
  
}
<?php

/**
 * Implementation of hook_views_plugins
 */
function view_event_views_plugins() {
  $path = drupal_get_path('module', 'view_event');
  $views_path = drupal_get_path('module', 'views');
  require_once "./$path/theme/theme.inc";
  return array(
    'module' => 'view_event',
    'style' => array(
      'view_event_style' => array(
        'title' => t('CTICFF Event View'),
        'help' => t('Displays nodes.'),
        'handler' => 'view_event_plugin_style',
        'path' => "$path/views",
        'theme' => 'view_event_view_style',
        'theme file' => 'theme.inc',
        'theme path' => "$path/theme",
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
    'row' => array(
      'view_event_row' => array(
        'title' => t('CTICFF Event Row'),
        'help' => t('(Displays a CTICFF Format Row'),
        'handler' => 'view_event_plugin_row',
        'path' => "$path/views",
        'theme' => 'view_event_view_row',
        'theme file' => 'theme.inc',
        'theme path' => "$path/theme",
        'uses fields' => TRUE,
        'type' => 'normal',
      ),
    )
  );
}

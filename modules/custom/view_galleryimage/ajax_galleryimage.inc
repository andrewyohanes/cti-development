<?php 
// $Id$
/**
 * @file
 */

function galleryimage_list($page) {
  $args = $_POST['args'];
  $filters = $_POST['filters'];
  $show_per_page = $_POST['show_per_page'];
  $sql = $_POST['query'];
  $view_toload = $_POST['view_name'];

  $startShow = ($page - 1) * $show_per_page;
  $result = array();
  
  if(!empty($filters)) {
    $sql .= " ORDER BY ";
    $i = 0;
    foreach($filters as $filter){
      if($i > 0){
        $sql .= ", ";
      }
      $sql .= $filter['name'] . ' ' . $filter['value'];
      $i++;
    }
  }
  $total_row = total_row($sql, $args, $show_per_page);
  $sql .= " LIMIT $startShow, $show_per_page";
  
  $output = let_view_show($view_toload, $sql, $page, $total_row, $show_per_page);
  
  header('Content-Type: application/json');
  $arrayOutput = json_decode(json_encode($output), True);
  echo json_encode($arrayOutput);
  exit;
}

function total_row($sql, $args, $show_per_page)
{
  $query = db_query($sql, $args);
  $result = array();
  while ($item = db_fetch_object($query)) {
    $result[] = $item;
  }
  
  return ceil(count($result) / $show_per_page);
}

function let_view_show($name, $sql, $number_active, $total_row, $show_per_page){
  $display_id = 'page_1';
  if ($view = collection_get_view($name, $sql)) {
    if ($view->access($display_id)) {

      // Fix 'q' for paging.
      if (!empty($path)) {
        $_GET['q'] = $path;
      }

      $view->dom_id = $dom_id;
      $errors = $view->validate();
      
      if ($errors === TRUE) {
        $object->status = TRUE;
        $object->display .= $view->preview($display_id, $args);
        if($total_row > $show_per_page) {
          $object->display .= my_pager($total_row, $number_active, $show_per_page);
        }
        $object->title = $view->get_title();
      }
      else {
        foreach ($errors as $error) {
          drupal_set_message($error, 'error');
        }
      }
      // Register the standard JavaScript callback.
      $object->__callbacks = array('Drupal.Views.Ajax.ajaxViewResponse');
      // Allow other modules to extend the data returned.
      drupal_alter('ajax_data', $object, 'views', $view);
    }
  }
  $messages = theme('status_messages');
  $object->messages = $messages ? '<div class="views-messages">' . $messages . '</div>' : '';

  return $object;
}

function my_pager($total_page, $number_active, $show_per_page) {
  $curr_start = 1;
  $curr_end = 9;
  if($number_active > 5){
    $curr_start = $number_active - 5;
    $curr_end = $number_active + 5;
  }
  
  $li = '';
  if($number_active > 1) {
    $li .= '<li class="pager-first first"><a href="#" title="Go to first page" class="pg_link active" data-page="1">« first</a></li>';
    $li .= '<li class="pager-previous"><a href="#" title="Go to previous page" class="pg_link active" data-page="'.($number_active - 1).'">‹ previous</a></li>';
  }
  
  if($number_active > 5){
    $li .= '<li class="pager-ellipsis">…</li>';
  }
  $cls = '';

  for ($i=$curr_start; $i <= $curr_end; $i++) {
    if($number_active == $i){
      $cls = 'active';
    }
    $li .= '<li class="pager-item '.$cls.'"><a href="#" title="Go to page '.$i.'" class="pg_link active" data-page="'.$i.'">'.$i.'</a></li>';
    $cls = '';
  }
  
  if($total_page - $number_active > 5) {
    $li .= '<li class="pager-ellipsis">…</li>';
  }
  
  if($total_page > 9) {
    $li .= '<li class="pager-next"><a href="/resources-page?page=1" title="Go to next page" class="active" data-page="'.($number_active + 1).'">next ›</a></li>';
    $li .= '<li class="pager-last last"><a href="/resources-page?page=37" title="Go to last page" class="active" data-page="'.$total_page.'">last »</a></li>';
  }
  
  $item_list = '<div class="item-list"><ul class="pager">'.$li.'</ul></div>';
  
  return $item_list;
}

function collection_get_view($name, $sql) {
  require_once drupal_get_path('module', 'view_galleryimage') . "/view_galleryimage.class.inc";

  $view = view_galleryimage::load($name);
  $default_view = views_get_default_view($name);

   // The view does not exist.
  if (empty($view) && empty($default_view)) {
    return;
  }
  // The view is defined in code.
  elseif (empty($view) && !empty($default_view)) {
    $status = variable_get('views_defaults', array());
    if (isset($status[$default_view->name])) {
      $default_view->disabled = $status[$default_view->name];
    }
    $default_view->type = t('Default');
    return $default_view->clone_view();
  }
  // The view is overriden/defined in the database.
  elseif (!empty($view) && !empty($default_view)) {
    $view->type = t('Overridden');
  }
  $view = $view->update_query_result($sql);

  return $view;
  
}
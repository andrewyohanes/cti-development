<?php
$country_arr = array();
$newArrayCountry = array();
$totalData = 0;

foreach($view->result as $result) {
  $node = node_load($result->nid); 
  foreach($node->taxonomy as $taxo) {
    if($taxo->vid == 6) {
      $term_name = taxonomy_get_term($taxo->tid);
      $country_arr[$taxo->tid][] = $node->nid;
    }
  }
}

ksort($country_arr, SORT_NUMERIC);
?>

<div class="view-content">
        
        <div id="rl_common_container"> <!--Start of rl_common_container class -->
            <?php
                
                $fileName= '';
                $totalNode = 0;
                $lastCountryId = 0;
                $lastNodeIndex = 0;
                //Assign the values for the pagination
                $pageKey = 'pg';
                $countryIndexKey = 'c';
                $nodeIndexKey = 'id';
                $tabId = '';
                
                $keys = array_keys($country_arr);
                $countryFirstId = $keys[0];
                $countryLastIdValue = $countryFirstId;
                $nodIdLastIndexValue = 0;
                
                if(isset($_GET[$countryIndexKey])) $countryLastIdValue = trim(mysql_escape_string($_GET[$countryIndexKey]));                
                if(isset($_GET[$nodeIndexKey]))  $nodIdLastIndexValue = trim(mysql_escape_string($_GET[$nodeIndexKey]));
                
                foreach($country_arr as $country => $node_data){
                 $newArrayCountry[] =  $country;
                }
                
                $lastIndexValue = count($newArrayCountry);
                
                $key = array_search($countryLastIdValue, $newArrayCountry);
                $countryTotal = count($country_arr);                
                $country_arr_new = array_slice($country_arr, $key, $countryTotal, true);
                
                //The main array is here
                foreach($country_arr_new as $country => $node_data){
                  
                    if($totalNode > 9){
                     break;
                    }
                    
                    $totalNodeValue = count($node_data);
                    if($totalNodeValue == 0){
                     continue;
                    }
                    
                    $countryLastIdValue = intval($countryLastIdValue);
                    $countryNew = intval($country);
                    
                    $lastCountryId = $country;
                      
                    $term_name = taxonomy_get_term($country);
                    $country = $term_name->name;
                    echo "<div class='countryName'><h3>" . $country . "</h3></div>";
                    
                    foreach($node_data as $indexValue => $nid ) {
                          $nodIdLastIndexValue = intval($nodIdLastIndexValue);
                          $indexValue = intval($indexValue);   
                          
                         /* if($nodIdLastIndexValue >= $indexValue){
                            continue;
                          }*/
                          
                          $lastNodeIndex = $indexValue;
                          
                          $node = node_load($nid);
                          $imageVal = $node->files;
                          $fileName = '';
                          foreach($imageVal AS $fileVal){
                            if(($fileVal->filename) != '')  $fileName .= $fileVal->filename;                      
                          }
                           //Get the image name from file extension
                          $imageName = getImageNameWithFileExtenstion($fileName);
                          //Here is the changes for the alt and title attribute of image
                          $imageAltTag = $node->field_image[0]['data']['alt'];
                          if(!empty($imageAltTag)){
                            $alt = $title = $imageAltTag;
                          } else {
                            $alt = $title = $node->title;;
                          }
                          
                          $authorName = $node->field_institutional_author[0]['value'];
                          $dateValue = $node->field_document_date[0]['value'];
                          $get_date=strtotime($dateValue);
                          
                          ?>
                          
                          <div class="rl_commonExeptWidth rl_eachContents">
                              <div class="rl_commonExeptWidth rl_leftImage">
                                  <a href="<?php print base_path().$node->path;?>" title="<?php print $title;?>"><img  alt="<?php print $alt;?>" src="<?php print base_path().path_to_theme();?>/css/images/<?php echo $imageName;?>.png" /></a>
                              </div><!--End of rl_leftImage class -->
                              
                              <div class="rl_commonExeptWidth rl_rightContainer">
                                  <div class="rl_title">                    
                                      <?php     
                                      $titleRl = ucfirst($node->title);
                                      echo $titleValue = l($titleRl,$node->path);
                                      ?>
                                  </div><!--End of rl_title class -->
                                  
                                  <div class="content_file_links">
                                    <?php
                                      $field_file_location_url = $node->field_file_location_url[0][url];
                                      $file_location_url = "<li><a href='$field_file_location_url'>Download file from source</a></li>";
                                         
                                      $files = $node->files;
                                      $file_url = '';
                                      foreach($files as $file){
                                          $file_path_for_download = $file->filepath;
                                          $filepath=$file->filepath;
                                          $file_type=explode('.',$filepath);
                                          $filetype_disp=strtolower($file_type[count($file_type)-1]);
                                          
                                          if($filetype_disp=='pdf'){
                                            $img_disp='<img src="'.base_path().path_to_theme().'/css/doc_types/icon_'.$filetype_disp.'.gif" title="'.$title.'" alt="document_'.$filetype_disp.'">';
                                          }
                                          else if($filetype_disp=='doc' || $filetype_disp=='docx'){
                                            $change_type='doc';
                                            $img_disp='<img src="'.base_path().path_to_theme().'/css/doc_types/icon_'.$change_type.'.gif" title="'.$title.'" alt="document_'.$filetype_disp.'">';
                                          }
                                          else {
                                            $change_type='generic';
                                            $img_disp='<img src="'.base_path().path_to_theme().'/css/doc_types/icon_'.$change_type.'.gif" title="'.$title.'" alt="document_'.$filetype_disp.'">';
                                          }
                                          
                                          if(!(stripos($file_path_for_download,'&'))){
                                            $file_download_path = base_path().drupal_get_path('module', 'pubdlcnt') .'/pubdlcnt.php?file='.base_path().$file_path_for_download.'&nid='.$node->nid;
                                          } else {
                                            $file_download_path = base_path().$file_path_for_download;
                                          }
                                          
                                          $file_url .= $img_disp.' <a href="'.$file_download_path.'" title="'.$title.'"><b>Download File</b></a>';
                                      }
                                      
                                      $readmore = "<a href='".base_path()."$node->path'>Read More</a>";
                                      echo "<ul>";
                                      echo "<li>$readmore</li>";                                 
                                      if($field_file_location_url != '')
                                        //echo "<li>$file_location_url</li>";
                                        echo "</ul>";
                                       
                                    ?>
                                    
                                  </div>
                                        
                                  <?php if(count($files)>0){ ?>
                                    <div class="contentDetail authorView">
                                        <?php print $file_url;?>        
                                    </div>
                                  <?php } ?>
                                  
                                  <?php if(!empty($authorName)){ ?>
                                    <div class="contentDetail authorView">
                                        <b>Author</b> : 
                                        <?php print $authorName;?>        
                                    </div>
                                  <?php } ?>
                                  
                                  <?php if(!empty($dateValue)){ ?>
                                    <div class="contentDetail authorView">
                                        <b>Date</b> : 
                                        <?php echo date('d-m-Y',$get_date);?>        
                                    </div>
                                  <?php } ?>
                                  
                                  <?php if ($node->taxonomy): ?>
                                    <div class="rl_commonExeptWidth rl_texonomyCont">
                                      <div class="taxonomy">
                                        <?php
                                             print display_cea_terms($node, $vid = NULL, $unordered_list = true);
                                        ?>
                                      </div>
                                    </div>
                                  <?php endif;?><!--End of rl_taxonomy class -->
                                  
                                  
                              </div> <!--End of rl_rightContainer class -->
                              
                              
                          </div><!--End of rl_eachContents class -->
                          
                          <?php
                          $totalNode++;
                          if($totalNode > 9){
                            $countryLastIdValue = $lastCountryId;
                            $nodIdLastIndexValue = $lastNodeIndex;
                            break;
                          
                          } else {
                            $nodIdLastIndexValue = 0;
                          }
                      }
                }
                
            ?>
        </div><!--End of rl_common_container class -->
        
        <div class="item-list">
          <?php
           if($totalData>9){
            print pagination_custom(10, $totalData, $country_arr, $pageKey, $countryIndexKey, $nodeIndexKey);
            }
          ?>
        </div>
    </div>

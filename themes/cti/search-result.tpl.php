<?php
// $Id: search-result.tpl.php,v 1.1.2.1 2008/08/28 08:21:44 dries Exp $

/**
 * @file search-result.tpl.php
 * Default theme implementation for displaying a single search result.
 *
 * This template renders a single search result and is collected into
 * search-results.tpl.php. This and the parent template are
 * dependent to one another sharing the markup for definition lists.
 *
 * Available variables:
 * - $url: URL of the result.
 * - $title: Title of the result.
 * - $snippet: A small preview of the result. Does not apply to user searches.
 * - $info: String of all the meta information ready for print. Does not apply
 *   to user searches.
 * - $info_split: Contains same data as $info, split into a keyed array.
 * - $type: The type of search, e.g., "node" or "user".
 *
 * Default keys within $info_split:
 * - $info_split['type']: Node type.
 * - $info_split['user']: Author of the node linked to users profile. Depends
 *   on permission.
 * - $info_split['date']: Last update of the node. Short formatted.
 * - $info_split['comment']: Number of comments output as "% comments", %
 *   being the count. Depends on comment.module.
 * - $info_split['upload']: Number of attachments output as "% attachments", %
 *   being the count. Depends on upload.module.
 *
 * Since $info_split is keyed, a direct print of the item is possible.
 * This array does not apply to user searches so it is recommended to check
 * for their existance before printing. The default keys of 'type', 'user' and
 * 'date' always exist for node searches. Modules may provide other data.
 *
 *   <?php if (isset($info_split['comment'])) : ?>
 *     <span class="info-comment">
 *       <?php print $info_split['comment']; ?>
 *     </span>
 *   <?php endif; ?>
 *
 * To check for all available data within $info_split, use the code below.
 *
 *   <?php print '<pre>'. check_plain(print_r($info_split, 1)) .'</pre>'; ?>
 *
 * @see template_preprocess_search_result()
 */

  $queryValue = check_plain($_GET['q']);
  $queryData = explode('/',$queryValue);
  $queryDataTotal = count($queryData);
  $searchKeyword = $queryData[$queryDataTotal - 1];
  $nodeValues = $result['node'];
  
  $nodeData = node_load($nodeValues->nid);
  
  $url = $nodeData->path;
  
  $title = $nodeValues->title;
  $bodyValue = $nodeValues->body;  
  $authorName = $nodeValues->field_institutional_author[0]['value'];
  $dateValue = $nodeValues->field_document_date[0]['value'];
  $get_date=strtotime($dateValue);
  
  $bodyValue = emphasize_specific_words($bodyValue, $searchKeyword, 'bold');
  $bodyValue = _substr($bodyValue, 400, 10);
  
  $node_type = $nodeValues->type;
  
  switch($node_type) {
    case 'document':
      $content_type = 'Document';
      break;
    
    case 'news':
      $content_type = 'News';
      break;
    
    case 'event':
      $content_type = 'Event';
      break;
    
    case 'page':
      $content_type = 'Page';
      break;
    
    case 'country':
      $content_type = 'Country';
      break;
    
    case 'partner':
      $content_type = 'Partner';
      break;
    
    case 'question_center':
      $content_type = 'Question';
      break;
  }
  
  if ($node_type != 'homepage_banner' && $node_type != 'profile' && $node_type != 'rotor_item' && $node_type != 'resource_type_description' && $node_type != 'story' && $node_type != 'webform') {
?>  
  <div class="resultContainer">
    <div class="title">
      <?php print l(html_entity_decode(str_replace("&#039;", "'", $title)), $url); ?>
      <br/>
      <span style="font-weight: normal"><?php print 'Type: ' . $content_type;?></span>
    </div>
        
    <div class="snippetBody">
        <p class="search-snippet"><?php print strip_tags($bodyValue) . ' ' . l('More >>', $url); ?></p>
    </div>
  </div>
  
  <?php
  }
  ?>
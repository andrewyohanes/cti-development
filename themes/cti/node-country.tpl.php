<?php
  $pagePath = $node->path;
  $flag_check = 0;
  if($pagePath == 'events'){
    $flag_check = 1; 
  }
?>

<div class="node <?php print $classes; ?>" id="node-<?php print $node->nid; ?>">
  <div class="node-inner">
    <?php if (!$page): ?>
      <h2 class="title"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>
    
    <div class="content">
      <div class="node_body_data">
      <div class="image_content">
      <?php
        $country_flag_path = $node->field_country_flag[0]['filepath'];
        $country_flag = theme('imagecache', 'country_flag_node', $country_flag_path);
        print $country_flag;
      ?>
      
        <?php          
          $url_path = explode('/',$node->path);
          $view_args = array($url_path[1]);
          $display_id = 'block_1';
          $view = views_get_view('country_page_resouces');
          $display_data = $view->execute_display($display_id , $view_args);
          if (!empty($display_data)) {
            print '<div class="news_section">';
            print '<h3>' . $node->title . ' News </h3>';
            print $display_data['content'];
            print '</div>';
          }
        ?>
      
      
        <?php          
          $url_path = explode('/',$node->path);
          $view_args = array($url_path[1]);
          $display_id = 'block_2';
          $view = views_get_view('country_page_resouces');
          $display_data = $view->execute_display($display_id , $view_args);
          if (!empty($display_data)) {
            print '<div class="events_section">';
            print '<h3>' . $node->title . ' Events </h3>';
            print $display_data['content'];
            print '</div>';
          }
        ?>
      
      
        <?php          
          $url_path = explode('/',$node->path);
          $view_args = array($url_path[1]);
          $display_id = 'block_3';
          $view = views_get_view('country_page_resouces');
          $display_data = $view->execute_display($display_id , $view_args);
          if (!empty($display_data)) {
            print '<div class="documents_section">';
            print '<h3>' . $node->title . ' Documents </h3>';
            print $display_data['content'];
            print '</div>';
          }
        ?>
      
      </div>
      <?php print $node->content['body']['#value']; ?>
      
      <?php
        $country_url = $node->field_country_url[0]['url'];
        
        if (!empty($country_url)) {
          print l('Visit Country Page', $country_url, array('attributes' => array('target' => '_blank')));
        }
      ?>
      </div>
    </div>

    <?php if ($terms): ?>       
         <div class="taxonomy">
           <?php
                //print display_cea_terms($node, $vid = NULL, $unordered_list = true);
           ?>
           <div style="clear:both"></div>
         </div>       
     <?php endif;?>
     <div class="page_links">
          <?php if ($links): ?> 
            <div class="links"> <?php print $links; ?></div>
          <?php endif; ?>
     </div>

  </div> <!-- /node-inner -->
  
  <?php
    if($flag_check != 1){
  ?>
    <div class="print_Ver">        
      <?php print l('<img src="'.base_path().path_to_theme().'/css/images/print_icon.gif
                    " title="Printer-friendly version" alt="Printer-friendly version" />Printer-friendly version', "print/".$node->nid, array('html' => true, 'attributes' => array('target' => '_blank')));?>
    </div>
  <?php
    }
  ?>
  
</div> <!-- /node-->


<?php
$node = $print['node'];
$nid = $node->nid;
// $Id: print.tpl.php,v 1.8.2.15 2009/07/09 12:00:52 jcnventura Exp $

/**
 * @file
 * Default print module template
 *
 * @ingroup print
 */

if($node->type=='event') {
  $startDate = $node->field_start_date[0]['value'];
  $startDate = date("j F Y",strtotime($startDate));
    
  if($node->field_end_date[0]['value']) {
    $endDate = $node->field_end_date[0]['value'];
    $endDate = date("j F Y",strtotime($endDate));
  }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $print['language']; ?>" xml:lang="<?php print $print['language']; ?>">
  <head>
    <?php print $print['head']; ?>
    <title><?php print $print['title']; ?></title>
    <?php print $print['scripts']; ?>
    <?php print $print['robots_meta']; ?>
    <?php print $print['base_href']; ?>
    <?php print $print['favicon']; ?>
    <?php print $print['css']; ?>
    <link rel="stylesheet" type="text/css" media='all' href="<?php print base_path().path_to_theme();?>/css/print.css" />
  </head>
  <body<?php print $print['sendtoprinter']; ?>>
    <?php if (!empty($print['message'])) {
      print '<div class="print-message">'. $print['message'] .'</div><p />';
    } ?>    
    <div class="print-site_name"><?php print $print['site_name']; ?></div>
    <p />
    <div class="print-breadcrumb"><?php print $print['breadcrumb']; ?></div>
    <hr class="print-hr" />
    <h1 class="print-title"><?php print $print['title']; ?></h1>
    <div class="print-content">   
      <div class="body_txt">
        <?php
          if($node->type=='event') {
          ?>
            <div class="common"><!--Date Data-->
              <div class="rl_commonExeptWidth rl_texonomyCont">
                <div class="fieldDateLabel"><b>Date</b>: &nbsp;<?php print $startDate; if($endDate)print ' - '.$endDate;?></div>
              </div>
            </div><!--End of Date data-->
          <?php
          }
        ?>
        <?php print $node->content['body']['#value'];?>
      </div>
      
        <?php
        if(!empty($node->field_version_pub[0]['value'])){
        ?>
        <div class="node_fields">
          <span class="node_label">Version note: </span>
          <?php            
            print $node->field_version_pub[0]['value'];
          ?>
        </div>   
        <?php
        }
        ?>
        
        <?php
        if(!empty($node->field_document_date[0]['value'])){
        ?>
        <div class="node_fields">
          <span class="node_label">Document Date: </span>
          <?php                     
            print $node->field_document_date[0]['view']
          ?>
        </div>   
        <?php
        }
        ?>
        
        <?php
        if(!empty($node->field_institutional_author[0]['value'])){
        ?>
        <div class="node_fields">
          <span class="node_label">Institutional Author: </span>
          <?php            
            print $node->field_institutional_author[0]['value'];
          ?>
        </div>   
        <?php
        }
        ?>
        
        <?php
        if(!empty($node->field_executive_summary[0]['value'])){
        ?>
        <div class="node_fields">
          <span class="node_label">Executive Summary: </span>
          <?php            
            print $node->field_executive_summary[0]['value'];
          ?>
        </div>   
        <?php
        }
        ?>
        
        <?php
        if($node->type !='event') {
          if ($node->taxonomy): ?>
        <div class="rl_commonExeptWidth rl_texonomyCont">
          <div class="taxonomy">
            <?php print $print['taxonomy'];?>
          </div>
        </div>
      <?php endif;
      }
      ?><!--End of rl_taxonomy class -->
      
      <?php
      if($node->type=='event') {
        
        $terms=$node->taxonomy;
                
                foreach($terms AS $termValue){
                	$subjectTerm = arg(1);
                	$term_tid = $termValue->tid;
                        $temp_display = views_embed_view('event_all_view', 'block_3', $term_tid);
                	$display5 .= $temp_display;
                }
                if($display5) { ?> 
               <div class="event_related_docs">
                	<div class="event_related_links">
                	<?php print $display5; ?>
               		</div>
               </div>
		<?php }
      }
      ?>
    </div>
    <hr class="print-hr" />
    <div class="print-source_url"><?php print $print['source_url']; ?></div>
  </body>
</html>

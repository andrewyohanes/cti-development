   $(document).keypress(function(event) {
      if (event.which == 95)
       {
           loadPopup();
       }
       else if(event.which == 27){
           disablePopup();
       }
   });
   

	 //Function to set default select option value for options list
	 function setSelect(id, val) {
			elem = document.getElementById(id);
			
			if(elem) {
				 total_options = elem.length;
				 
				 var selected_index = document.getElementById(id).selectedIndex;
							 
				 for (i = 0; i<total_options; i++) {
						if(selected_index) {
							 break;
						}
						else if(elem[i].value == val) {
							 elem[i].selected = true;
							 break;
						}
				 }
			}
	 }
   
   //facebook share functionality
   $(document).ready(function() {
      $('.share_facebook').click(function() {
         var shareLink = window.location;
         window.open("http://facebook.com/sharer.php?u="+shareLink+e,"facebook","height=340,width=660,resizable=1");
      });
      $('.share_twitter').click(function() {
         var shareLink = jQuery('#content-header h1.title').html();
         shareLink = shareLink.replace(' ', '+');
         window.open("https://twitter.com/intent/tweet?text="+shareLink,"twitter","height=340,width=660,resizable=1");
      });
   });

   //This is only for the search function
   $(document).ready(function(){
       $("#edit-keys").click(function(){
        var searchVal = $("#edit-keys").val();
          searchVal = jQuery.trim(searchVal);
           if(searchVal == 'Search'){
               $("#edit-keys").val('');
                $("#edit-keys").css("color","#7196B8");
           }
           else{
            $("#edit-keys").val(searchVal);
            $("#edit-keys").css("color","#7196B8");
           }
         });
       $("#edit-keys").blur(function(){
           var searchVal = $("#edit-keys").val();
           searchVal = jQuery.trim(searchVal);
           if(searchVal == ''){
               $("#edit-keys").val('Search');
               $("#edit-keys").css("color","#c0c0c0");
           }
       });
   });
   
   //Codes to manipulate Sub-menus on the Top Nav Bar
   $(document).ready(function() {
      jQuery('li.menu-mlid-1527').hover(
         function() {
            jQuery(this).children('.menu').show();
         },
         function() {
            jQuery(this).children('.menu').hide();
         }
      );
      jQuery('li.menu-mlid-1526').hover(
         function() {
            jQuery(this).children('.menu').show();
         },
         function() {
            jQuery(this).children('.menu').hide();
         }
      );
      jQuery('li.menu-mlid-1837').hover(
         function() {
            jQuery(this).children('.menu').show();
         },
         function() {
            jQuery(this).children('.menu').hide();
         }
      );
      jQuery('li.menu-mlid-5850').hover(
         function() {
            jQuery(this).children('.menu').show();
         },
         function() {
            jQuery(this).children('.menu').hide();
         }
      );
      jQuery('li.menu-mlid-5833').hover(
         function() {
            jQuery(this).children('.menu').show();
         },
         function() {
            jQuery(this).children('.menu').hide();
         }
      );
      jQuery('li.menu-mlid-6012').hover(
         function() {
            jQuery(this).children('.menu').show();
         },
         function() {
            jQuery(this).children('.menu').hide();
         }
      );
      jQuery('a#dhtml_menu-1527').click(function() {
         window.location = '/about-us';
      });
      jQuery('a#dhtml_menu-1526').click(function() {
         window.location = '/news';
      });
      jQuery('a#dhtml_menu-1837').click(function() {
         window.location = '/events';
      });
      jQuery('a#dhtml_menu-5850').click(function() {
         window.location = '/cti-cff-collaboration';
      });
      jQuery('a#dhtml_menu-5833').click(function() {
         window.location = '/partners';
      });
      jQuery('a#dhtml_menu-6012').click(function() {
         window.location = '/countries';
      });
      if(window.location.pathname=='/cti-cff-regional-map' || window.location.pathname=='/cti-cff-secretariat' || window.location.pathname=='/faq'){
         jQuery("a#dhtml_menu-1527").addClass('active-trail active');
      }
      if(window.location.pathname=='/cti-cff-photo-galleries'){
         jQuery("a#dhtml_menu-1526").addClass('active-trail active');
      }
      if(window.location.pathname=='/events' || window.location.pathname=='/previous-events'){
         jQuery("a#dhtml_menu-1837").addClass('active-trail active');
      }
      if(window.location.pathname=='/country/indonesia' || window.location.pathname=='/country/malaysia' || window.location.pathname=='/country/papua-new-guinea' || window.location.pathname=='/country/philippines' || window.location.pathname=='/country/solomon-islands' || window.location.pathname=='/country/timor-leste'){
         jQuery("a#dhtml_menu-6012").addClass('active-trail active');
      }
      if(window.location.pathname=='/how-become-involved'){
         jQuery("a#dhtml_menu-5833").addClass('active-trail active');
      }
      if(window.location.pathname=='/collaboration-coordination-mechanisms-and-implementation-partners' || window.location.pathname=='/collaboration-goals-and-commitments-action' || window.location.pathname=='/collaboration-financial-resources' || window.location.pathname=='/collaboration-monitoring-and-evaluation'){
         jQuery("a#dhtml_menu-5850").addClass('active-trail active');
      }
   });
   
   //This is only for the library page drop down
   $(document).ready(function(){
      var most_download_tab_index = 2;
      
      if(most_download_tab_index == 2){
         $('#titleOfExposeViewblock_1').click(function(){
            $("#views-exposed-form-resource-library-block-1").slideToggle("slow");
            exit;
         });
         
         $('#titleOfExposeViewblock_3').click(function(){
            $("#views-exposed-form-resource-library-block-3").slideToggle("slow");
            exit;
         });
      }
      
      $('#quicktabs-tab-9-0').click(function(){
          $("#views-exposed-form-resource-library-block-1 #edit-tid-68-wrapper").html('<label for="edit-tid-68" class="option"><input type="checkbox" value="68" id="edit-tid-68" name="tid[]"> Program Document</label>');
      });

      $('#quicktabs-tab-9-1').click(function(){
          $("#views-exposed-form-resource-library-block-3 #edit-tid-68-wrapper").html('<label for="edit-tid-68" class="option"><input type="checkbox" value="68" id="edit-tid-68" name="tid[]"> Program Document</label>');
      });
      
      $('#quicktabs_container_9').ajaxSuccess(function(evt,request,settings){
         $("#views-exposed-form-resource-library-block-1 #edit-tid-68-wrapper").html('<label for="edit-tid-68" class="option"><input type="checkbox" value="68" id="edit-tid-68" name="tid[]"> Program Document</label>');
         
         $("#views-exposed-form-resource-library-block-3 #edit-tid-68-wrapper").html('<label for="edit-tid-68" class="option"><input type="checkbox" value="68" id="edit-tid-68" name="tid[]"> Program Document</label>');
         
         
         $("#views-exposed-form-resource-library-block-1").css({'display':'none'});
         $("#views-exposed-form-resource-library-block-3").css({'display':'none'});
         
         $("#views-exposed-form-resource-library-block-1 #searchResetButton").click(function() {            
            $(':input','#views-exposed-form-resource-library-block-1').removeAttr('checked');
         });
         
         $("#views-exposed-form-resource-library-block-3 #searchResetButton").click(function() {
            $(':input','#views-exposed-form-resource-library-block-3').removeAttr('checked');
         });
           
         $('#titleOfExposeViewblock_1').click(function(){
            $("#views-exposed-form-resource-library-block-1").slideToggle("slow");
            exit;
         });
        
         $('#titleOfExposeViewblock_3').click(function(){
            $("#views-exposed-form-resource-library-block-3").slideToggle("slow");
            exit;
         });
      });
      
   });


   //Add an id to a tag
   $(document).ready(function() {
      $('.pagerOutput .item-list .pager li a').click(function(){
         $(".pagerOutput .item-list .pager a").each(function(){
            var quick_tab_value = $(this).attr('href');
            if(quick_tab_value.search('quicktabs_9')){
               $(this).attr('href', $(this).attr('href')+ '&quicktabs_9=2');
            } else if(quick_tab_value.search('quicktabs_10')){
               $(this).attr('href', $(this).attr('href')+ '&quicktabs_10=2');
            }
         });
      });
      
   
      var rotating_image = "";
  if($('#views-cycle-rotating_image-block_1-nav')) {
    rotating_image = $('#views-cycle-rotating_image-block_1-nav').html();
  }
  if( rotating_image != "" ) {
    $('#views-cycle-rotating_image-block_1-nav').prepend("<span id='arrow_home_feature_left_li'></span>");
    $('#views-cycle-rotating_image-block_1-nav').append("<span id='arrow_home_feature_right_li'></span>");
  }
    
  $('#arrow_home_feature_left_li').click(
    function(e){
      $('#views-cycle-rotating_image-block_1').cycle('prev');
    }
    );
  $('#arrow_home_feature_right_li').click(
    function(e){
      $('#views-cycle-rotating_image-block_1').cycle('next');
    }
    );
  
  var rotating_event="";
  if($('#views-cycle-rotating_event-block_1-nav')) {
    rotating_event = $('#views-cycle-rotating_event-block_1-nav').html();
  }
  if(rotating_event != "" ) {
    $('#views-cycle-rotating_event-block_1-nav').prepend("<span id='arrow_home_feature_event_left_li'></span>");
    $('#views-cycle-rotating_event-block_1-nav').append("<span id='arrow_home_feature_event_right_li'></span>");
  }
    
  $('#arrow_home_feature_event_left_li').click(
    function(e){
      $('#views-cycle-rotating_event-block_1').cycle('prev');
    }
    );
  $('#arrow_home_feature_event_right_li').click(
    function(e){
      $('#views-cycle-rotating_event-block_1').cycle('next');
    }
    );
 
 
 if(window.location.pathname=='/cti-cff-regional-map' || window.location.pathname=='/cti-cff-secretariat'){
   
   $("a#dhtml_menu-1527").addClass('active-trail active');
 }
      
   });
   
   function facebookpopup(e){
      window.open("http://facebook.com/sharer.php?u=http://pastebin.com"+e,"facebook","height=340,width=660,resizable=1");
   }
   
<?php
     //Here is the changes for the alt and title attribute of image
    $imageAltTag = $node->field_image[0]['data']['alt'];
    if(!empty($imageAltTag)){
      $alt = $title = $imageAltTag;
    } else {
      $alt = $title = $node->title;
    }
    $bodyValue = $node->content['body']['#value'];
    $imagePath = trim($node->field_image[0]['filepath']);
    $attachmentValue = $node->field_attachment;
    $startDate = $node->field_start_date[0]['value'];
    $startDate = date("j F Y",strtotime($startDate));
    
    if($node->field_end_date[0]['value']) {
		 $endDate = $node->field_end_date[0]['value'];
    	$endDate = date("j F Y",strtotime($endDate));
    }
?>  


<div class="node <?php print $classes; ?>" id="node-<?php print $node->nid; ?>">
    <div class="node-inner">
        <div id="rlLanding_common_container">          
          
          <div class="common"><!--Date Data-->
            <div class="rl_commonExeptWidth rl_texonomyCont">
              <div class="fieldDateLabel">Date: &nbsp; <?php print $startDate; if($endDate)print ' - '.$endDate;?></div>
            </div>
          </div><!--End of Date data-->
          
          <div class="rl_commonExeptWidth rl_eachContents">
              <?php if($imagePath != ''){ ?>
                <div class="rl_commonExeptWidth rl_leftImage">                  
                    <?php print theme('imagecache', 'node_page_image', $imagePath, $alt, $title); ?>
                </div><!--End of rl_leftImage class -->
              <?php } ?>
              
              <div class="rl_rightContainer">               
                
                <div class="rl_taxonomy">          
                  <?php print $bodyValue; ?>
                </div><!--End of rl_taxonomy class -->                
                
                    <?php
                      if($attachmentValue != ''){
                        foreach($attachmentValue AS $attachValue){
                          $attachedFilePath = $attachValue['filepath'];
                          $attachedFileName = trim($attachValue['filename']);                          
                        ?>                    
                          <div class="rl_taxonomy">
                            <?php
                              print 'Attached File: ' . $titleValue = l($attachedFileName, $attachedFilePath);
                            ?>
                          </div>
                          
                        <?php
                        }
                      }
                    ?>
               
                
              </div> <!--End of rl_rightContainer class -->
	      
              <?php 
                $terms=$node->taxonomy;
		
                foreach($terms AS $termValue){
                	$subjectTerm = arg(1);
                	$term_tid = $termValue->tid;
                        $temp_display = views_embed_view('event_all_view', 'block_3', $term_tid);
                	$display5 .= $temp_display;
                }
                if($display5) { ?> 
               <div class="event_related_docs">
                	<div class="event_related_links">
                	<?php print $display5; ?>
               		</div>
               </div>
		<?php } ?>
          </div><!--End of rl_eachContents class -->
        </div><!--End of the div rl_common_container -->
        
		<div class="node_navigate_links">
              <?php print l('Events Section','events',array('attributes'=>array('title'=>'Events Section'))); ?> |
                <a href="<?php print base_path();?>" title="Site home">Home</a>
            </div>
        
  </div> <!-- /node-inner -->
</div> <!-- /node-->


<div class="print_Ver">        
  <?php print l('<img src="'.base_path().path_to_theme().'/css/images/print_icon.gif
                " title="Printer-friendly version" alt="Printer-friendly version" />Printer-friendly version', "print/".$node->nid, array('html' => true, 'attributes' => array('target' => '_blank')));?>
</div>

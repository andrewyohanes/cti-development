<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

  <head>
    <title><?php print $head_title; ?></title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <!--[if lte IE 6]><style type="text/css" media="all">@import "<?php print $base_path . path_to_theme();?>/css/ie6.css";</style><![endif]-->
    <!--[if IE 7]><style type="text/css" media="all">@import "<?php print $base_path . path_to_theme();?>/css/ie7.css";</style><![endif]-->
    <!--[if IE 8]><style type="text/css" media="all">@import "<?php print $base_path . path_to_theme();?>/css/ie8.css";</style><![endif]-->
    <!--test-->
    
    <link rel="stylesheet" type="text/css" media="print" href="<?php print $base_path . path_to_theme();?>/css/print.css" />

    
    <?php print $scripts; ?>
    <script src="<?php print $base_path . path_to_theme() ?>/js/cti.js" type="text/javascript"></script>        
    <?php
    //Tweak for the Primakry Menu in the Secondary level pages
    if($node->type=='document') {
    ?>
      <script type="text/javascript">
      $(document).ready(function() {	
	$('#dhtml_menu-2868').addClass('active');
      });
      </script>
    <?
    }    
    if($node->type=='event') {
    ?>
      <script type="text/javascript">
      $(document).ready(function() {	
	$('#dhtml_menu-1837').addClass('active');
      });
      </script>
    <?    
    }
    if($node->type=='news') {
    ?>
      <script type="text/javascript">
      $(document).ready(function() {	
	$('#dhtml_menu-1526').addClass('active');
      });
      </script>
    <?
    }
    
    //Tweak Library page for View links
    $get_url=$_SERVER['REQUEST_URI'];
    if(stristr($get_url,'resource_by')){
    ?>
      <script type="text/javascript">
      $(document).ready(function() {	
	$('#dhtml_menu-2868').addClass('active');
      });
      </script>
    <?
    }else if(stristr($get_url,'cea_event')){
    ?>
      <script type="text/javascript">
      $(document).ready(function() {	
	$('#dhtml_menu-1837').addClass('active');
      });
      </script>
    <?
    }
    ?>
    <style type='text/css'>
    #content_page #content-inner {
    background-color: #FFFFFF;
    border: 1px solid #ADD8E6;
    float: left;
    margin-left: 10px;
    padding-bottom: 10px;
    padding-left: 10px;
    padding-right: 10px;
    width: 914px;
}
    </style>
  </head>

  <body class="cea-page <?php print $body_classes; ?>">
    <div style="display:none">
      <a href="#skipnavigation"></a>
    </div> 
    
    <div id="page">
    
    
    <!-- ______________________ HEADER _______________________ -->

    <div id="header">
      <div id="logo-title">
	    <?php if (!empty($logo)): ?>
          <a href="<?php print base_path();?>" title="<?php print $site_slogan; ?>" rel="home" id="logo">
          <img src="<?php print base_path().path_to_theme();?>/cti_logo.png" title="<?php print $site_slogan; ?>" alt="<?php print $site_slogan; ?>"/> 
	      </a>
        <?php endif; ?>
          
      </div> <!-- /logo-title -->
      <div id="site-header">
	<div class="site_search">
	    <div class="div_border">
	    <form class="search-form" id="search-form" method="post" accept-charset="UTF-8" action="<?php print base_path();?>search/apachesolr_search/">
		<!--div class="search_label">
		    Search Content
		</div-->
		<div id="search_cti_field" class="search_cti_box">
		    <div class="search_field_box">
			<input type="text" class="form-text search_cti" value="Search" id="edit-keys" name="keys"/>
		    </div>
		    <div class="search_field_button">
			<input id="edit-search" class="search-form-submit" name="op" type="submit" value="" />
		
		    </div>
		</div>
		<div class="search_cti_box">
		</div>
	    </form>
	    </div>
	  </div>
         <?php print $header_right; ?>
      </div> <!-- /site header -->
      <div class="clear_all"></div>
    </div> <!-- /header -->
    <a name="skipnavigation" title="skip navigation"></a>
    <!-- _________________________Nice Menus__________________________ -->
    
    <?php
      if (!empty($nice_menu)):
    ?>
      <div id="nice_menu">
	<div  class="nice_menu_links">
	    <?php print $nice_menu; ?>
	    <div class="clear_all"></div>
	</div>
      </div>
    <?php
      endif;
    ?>

    <!-- ______________________ MAIN _______________________ -->

    <div id="main" class="clearfix">	  
      
      
      <div id="content_page">
        <div id="content-inner" class="inner column center">

          <?php if ($content_top): ?>
            <div id="content-top">
              <?php print $content_top; ?>
            </div> <!-- /#content-top -->
          <?php endif; ?>
	  
	  <?php if ($content_middle): ?>
            <div id="content_middle">
              <?php print $content_middle; ?>
            </div> <!-- /#content_middle -->
          <?php endif; ?>
	  
          <?php if ($breadcrumb || $title || $messages || $help || $tabs): ?>
            <div id="content-header">

              <?php //print $breadcrumb; ?>
              <?php
			    //Reconstruct Country Node title
				$node_type = $node->type;
				
				if ($node_type == 'country') {
				  $title = t('CTI-CFF: ' . $title);
				}
			  ?>
              <?php if ($title): ?>
                <h1 class="title"><?php print $title; ?></h1>
              <?php endif; ?>

             

              <?php print $messages; ?>

              <?php print $help; ?> 

              <?php if ($tabs): ?>
                <div class="tabs"><?php print $tabs; ?></div>
              <?php endif; ?>

            </div> <!-- /#content-header -->
          <?php endif; ?>

          <div id="content-area">
            <?php print $content; ?>
          </div> <!-- /#content-area -->

          <?php //print $feed_icons; ?>

          <?php if ($content_bottom): ?>
            <div id="content-bottom">
              <?php print $content_bottom; ?>
            </div><!-- /#content-bottom -->
          <?php endif; ?>

        </div>
      </div> <!-- /content-inner /content -->

      </div> <!-- /main -->

      <!-- ______________________ FOOTER _______________________ -->

      
      <div id="footer">
	<!--<div id="footer_left" class="footer_layout">
	</div>-->
	<div id="footer_body" class="footer_layout">                
	    <?php print $footer_block; ?>
	</div>
	<!--<div id="footer_right" class="footer_layout">
	</div>-->
	<div class="clear_all"></div>
	<div id="footer_message">
	  <?php print $footer_message; ?>
	</div>
      </div> <!-- /footer -->

    </div> <!-- /page -->
    <?php print $closure; ?>
  </body>
</html>
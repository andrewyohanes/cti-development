<?php if (!empty($title)): ?>  
    <h3><?php print $title; ?></h3>  
<?php endif; ?>

<div class="view-content">
  <?php
      $resultData = $view->result;
      foreach($resultData AS $resKey => $val){
          $node = node_load($val->nid);
          
          //Here is the changes for the alt and title attribute of image
          $imageAltTag = $node->field_image[0]['data']['alt'];
          if(!empty($imageAltTag)){
            $alt = $title = $imageAltTag;
          } else {
            $alt = $title = $val->node_title;
          }
           
          
          $node->field_image[0]['data']['title'];
          $image_filepath = $node->field_image[0]['filepath'];
          $path='node/'.$val->nid;
          $attributes = '';
          ?>
              <div class="views-row">
                  <div class="views-field-field-image-fid">
                    <span class="field-content"><a title="<?php print $node->title; ?>" href="<?php print base_path().drupal_get_path_alias($path);?>"><?php print theme('imagecache', 'highlights_homepage_thumbnail_preset', $image_filepath, $alt, $title, $attributes); ?></a></span>
                  </div>
                  
                  <div class="views-field-title">
                    <span class="field-content"><a title="<?php print $node->title; ?>" href="<?php print base_path().drupal_get_path_alias($path);?>"><?php print $node->title; ?></a></span>
                  </div>
                  
                  <div class="views-field-body">
                    <div class="field-content">
                      <?php echo strip_tags(_substr($node->body,108,3));?><a title="<?php print $node->title; ?>" href="<?php print base_path().drupal_get_path_alias($path);?>">more&#187;</a>
                    </div>                        
                  </div>
                  
              </div>
              
          <?php
      }    
  ?>
</div><!--End of View Content-->
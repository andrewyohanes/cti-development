<script type="text/javascript" src="<?php print 'http://coraltriangleinitiative.org/'.path_to_theme(); ?>/js/highslide-full.js"></script>
<link rel="stylesheet" type="text/css" href="<?php print 'http://coraltriangleinitiative.org/'.path_to_theme(); ?>/css/highslide.css" />
<style type="text/css">
    .highslide-image {
        border: none;
    }
    .highslide-controls {
        width: 90px !important;
    }
    .highslide-controls .highslide-close {
        display: none;
    }
    .highslide-caption {
        padding-top: 10px;
        padding-right: 0;
        padding-bottom: 10px;
        padding-left: 0;
    }
</style>
<script type="text/javascript">
    hs.graphicsDir = '<?php print 'http://coraltriangleinitiative.org/'.path_to_theme(); ?>/css/graphics/';
    hs.transitions = ['expand', 'crossfade'];
    hs.restoreCursor = null;
    hs.lang.restoreTitle = 'Click for next image';

    // Add the slideshow providing the controlbar and the thumbstrip
    hs.addSlideshow({
        //slideshowGroup: 'group1',
        interval: 5000,
        repeat: true,
        useControls: true,
        overlayOptions: {
            position: 'bottom right',
            offsetY: 10
        },
        thumbstrip: {
            position: 'above',
            mode: 'horizontal',
            relativeTo: 'expander'
        }
    });
    var inPageOptions = {
        //slideshowGroup: 'group1',
        outlineType: null,
        allowSizeReduction: true,
        wrapperClassName: 'in-page controls-in-heading',
        thumbnailId: 'gallery-area',
        useBox: true,
        width: 610,
        height: 407,
        targetX: 'gallery-area 0px',
        targetY: 'gallery-area 0px',
        captionEval: 'this.a.title',
        numberPosition: 'caption'
    }
    // Open the first thumb on page load
    hs.addEventListener(window, 'load', function() {
        document.getElementById('thumb1').onclick();
    });

    // Cancel the default action for image click and do next instead
    hs.Expander.prototype.onImageClick = function() {
        if (/in-page/.test(this.wrapper.className))	return hs.next();
    }

    // Under no circumstances should the static popup be closed
    hs.Expander.prototype.onBeforeClose = function() {
        if (/in-page/.test(this.wrapper.className))	return false;
    }
    // ... nor dragged
    hs.Expander.prototype.onDrag = function() {
        if (/in-page/.test(this.wrapper.className))	return false;
    }

    // Keep the position after window resize
   /* hs.addEventListener(window, 'resize', function() {
        var i, exp;
        hs.getPageSize();

        for (i = 0; i < hs.expanders.length; i++) {
            exp = hs.expanders[i];
            if (exp) {
                var x = exp.x,
                    y = exp.y;

                // get new thumb positions
                exp.tpos = hs.getPosition(exp.el);
                x.calcThumb();
                y.calcThumb();

                // calculate new popup position
                x.pos = x.tpos - x.cb + x.tb;
                x.scroll = hs.page.scrollLeft;
                x.clientSize = hs.page.width;
                y.pos = y.tpos - y.cb + y.tb;
                y.scroll = hs.page.scrollTop;
                y.clientSize = hs.page.height;
                exp.justify(x, true);
                exp.justify(y, true);

                // set new left and top to wrapper and outline
                exp.moveTo(x.pos, y.pos);
            }
        }
    });*/

</script>

<div class="node <?php print $classes; ?>" id="node-<?php print $node->nid; ?>">
  <div class="node-inner">
    <?php if (!$page): ?>
      <h2 class="title"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>
    
    <div class="content">
      <div class="node_body_data highslide-gallery " id="gallery-area">
      <?php
      $results = $view->result;
      $i = 0;
      foreach(  $results  as  $photo) {
            //print '<pre>';
            //print_r($photo);
            //print '</pre>';
            //exit();
            $node = node_load($photo->nid);
            ////print '<pre>';
            ////print_r($node);
            ////print '</pre>';
            ////exit();
            //$alt_text = $node->field_photo_browsing[0]['data']['title'];
            $caption_text = $node->field_caption[0]['value'];
            $credit_text = $node->field_credit[0]['value'];
          $fid = $photo->node_data_field_photo_browsing_field_photo_browsing_fid;
          $my_array = field_file_load($fid);
          $get_image = getimagesize($my_array['filepath']);
          //print '<pre>';
          //print_r($get_image);
          //print '</pre>';
          //exit();
          $partner_logo = theme('imagecache', 'partner_logo', $my_array['filepath']);
          if ($get_image[0] > $get_image[1]) {
            $partner_logo2 = theme('imagecache', '618x620', $my_array['filepath']);
          } else {
            $partner_logo2 = theme('imagecache', 'Portrait', $my_array['filepath']);
          }
          preg_match( '/src="([^"]*)"/i', $partner_logo2, $array ) ;
          // print_r( $array[1] ) ;
          //$str = explode("src=", $partner_logo2);
         // print $str[1];
          if($i == 0) {
              print  "<a title='" . '<span class="photo-caption">' . $caption_text . '</span>' . '</br>' . '<span class="photo-credit">Photo: ' . $credit_text . '</span>' . "' id='thumb1' class='highslide'  onclick='return hs.expand(this, inPageOptions)' href='" .  $array[1] ."'>" . $partner_logo ."</a>";
          }
          else {
              print  "<a title='" . '<span class="photo-caption">' . $caption_text . '</span>' . '</br>' . '<span class="photo-credit">Photo: ' . $credit_text . '</span>' . "' class='highslide'  onclick='return hs.expand(this, inPageOptions)' href='" . $array[1] ."'>" . $partner_logo ."</a>";
          }

      }
      ?>
    </div>
        </div>
</div>
    </div>
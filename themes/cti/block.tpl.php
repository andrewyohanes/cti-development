<div id="block-<?php print $block->module .'-'. $block->delta ?>" class="<?php print $block_classes . ' ' . $block_zebra; ?>">
  <div class="block-inner">
    <?php
    //Change page title for Taxonomy Term page
    $get_url_data=explode('/',$_SERVER['REQUEST_URI']);
    
    //pr_disp($get_url_data);
    //pr_disp($block);
    
    $get_block_id=$block->bid;    
    $get_type=$get_url_data[1];
    $get_taxonomy_tid=$get_url_data[2];;
    
    $get_term_detials=taxonomy_get_term($get_taxonomy_tid);
    $vocabulary = taxonomy_vocabulary_load($get_term_detials->vid);
      
    if(($get_type=='resource_by_subject') && ($get_block_id==567)){
      $page_title='Resources by Subject: '.$get_term_detials->name;    
    }
    else if(($get_type=='resource_by_type') && ($get_block_id==568)){
      $page_title='Resources by Type: '.$get_term_detials->name;  
    }
    else if($get_type=='resource_by_country' && $get_block_id==569){
	$page_title='Resources by Country: '.$get_term_detials->name;    
    }
    
    
    ?>
    <?php
      if (!empty($page_title)){?>    
	<h3 class="title block-title"><?php print $page_title; ?></h3>
    <?php
      }
      else if (!empty($block->subject)){
      ?>    
	<h3 class="title block-title"><?php print $block->subject; ?></h3>
    <?php
      }
    ?>

    <div class="content">
      <?php print $block->content; ?>
    </div>

    <?php print $edit_links; ?>

  </div> <!-- /block-inner -->
</div> <!-- /block -->
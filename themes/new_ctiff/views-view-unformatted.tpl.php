<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */

global $base_url;
global $base_path;

$breadcrumb = array();
$breadcrumb[] = l('Home', '');
$breadcrumb[] = l(drupal_get_title(), $base_url . base_path() . $view->display_handler->options['path']);

// Set Breadcrumbs
$breadcrumbs = drupal_set_breadcrumb($breadcrumb);
$title = $view->display_handler->options['title'];
?>
<div class="views-unformatted-style">
  <div class="entry-header">
    <div class="content-header">
      <div class="breadcrumb">
        <ol class="breadcrumb">
          <?php foreach($breadcrumbs as $breadcrumb): ?>
            <li><?php echo $breadcrumb; ?></li>
          <?php endforeach; ?>
        </ol>
      </div>
      <?php if (!empty($title)) : ?>
        <h1><?php print $title; ?></h1>
      <?php endif; ?>
    </div>
  </div>
  <div class="container-fluid">
    <?php foreach ($rows as $id => $row): ?>
      <div class="<?php print $classes[$id]; ?>">
        <?php print $row; ?>
      </div>
    <?php endforeach; ?>
  </div>
</div>
<div class="modal fade" id="img-modal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" data-dismiss="modal">&times;</button>
        <h4 id="img-title"></h4>
      </div>
      <div class="modal-body">
        <img class="thumbnail" id="preview-img" src=""></img>
      </div>
    </div>
  </div>
</div>
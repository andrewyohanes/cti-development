<?php
/**
 * @file views-view-list.tpl.php
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */

global $base_url;
global $base_path;

$breadcrumb = array();
$breadcrumb[] = l('Home', '');
$breadcrumb[] = l(drupal_get_title(), $base_url . base_path() . $view->display_handler->options['path']);

// Set Breadcrumbs
$breadcrumbs = drupal_set_breadcrumb($breadcrumb);
$title = $view->display_handler->options['title'];
?>
<style>
  section .entry-header h1 {
    padding: 0 0 25px 0;
    margin: 0;
    color: #fff;
    font-weight: bold;
    font-size: 40px;
    line-height: 45px;
  }
  .owl-carousel .owl-item img {
    width: auto;
  }
  .views-field-field-flag-country-fid {
    display: inline-block;
    float: left;
    margin-right: 30px;
  }
  .views-field-title {
    margin-bottom: 15px;
    font-size: 40px;
    line-height: 45px;
  }
  .views-field-body {
    display: block;
    float: left;
    margin-top: 15px;
  }
  section .entry-nav.entry-nav-event-page {
      display: inline-block;
      position: absolute;
      top: 268px;
      left: 15px;
      z-index: 10;
  }
</style>
<section>
<div class="views-list">
  <div class="entry-header">
    <div class="content-header">
      <div class="breadcrumb">
        <ol class="breadcrumb">
          <?php foreach($breadcrumbs as $breadcrumb): ?>
            <li><?php echo $breadcrumb; ?></li>
          <?php endforeach; ?>
        </ol>
      </div>
      <?php if (!empty($title)) : ?>
        <h1><?php print $title; ?></h1>
      <?php endif; ?>
    </div>
  </div>
  <div class='entry-content'>
    <div class='container' style="position: relative;">
      <div class='entry-nav entry-nav-event-page'>
        <div class='owl-nav' id='customNavEventPage'></div>
        <div class='owl-dots' id='customDotsEventPage'></div>
      </div>
      <div class='owl-carousel owl-theme public-holiday' id='event-page-feature'>
        <?php foreach ($rows as $id => $row): ?>
          <div class='slide'>
            <div class='row-event'>
              <?php print $row; ?>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
</div>
</section>


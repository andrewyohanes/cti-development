<button class="btn btn-info" onclick="goToTop();" id="top-btn"><i class="fa fa-angle-up"></i></button>
    <!-- end of top btn -->
<div class="container-fluid">
        <div class="row text-center">
            <div class="col-xs-12 col-sm-2 col-md-2">
                <span class="" data-toggle="modal" data-target="#finance-manager">
                    <img src="http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4231(1).jpg" alt="" class="img-thumbnail img-responsive">
                </span>
                <p>
                    <span class="profile-header">Cepy F. Syahda</span>
                    <em><small>Finance &amp; Administration Sr. Manager</small></em>
                </p>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2">
                <span class="" data-toggle="modal" data-target="#program-manager">
                    <img src="http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4334(1).jpg" alt="" class="img-thumbnail">
                </span>
                <p>
                    <span class="profile-header">Muhammad Lukman</span>
                    <em><small>Technical Program Sr. Manager</small></em>
                </p>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2">
                <span class="my-hover-dark" data-toggle="modal" data-target="#twg-manager">
                    <img src="http://coraltriangleinitiative.org/sites/default/files/resources/twg-senior-manager_0.jpg" alt="" class="img-thumbnail">
                </span>
                <p>
                    <span class="profile-header">Romina Astrid Lim</span>
                    <em><small>Technical Working Group Sr. Manager</small></em>
                </p>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2">
                <span class="my-hover-dark" data-toggle="modal" data-target="#gwg-manager">
                    <img src="http://coraltriangleinitiative.org/sites/default/files/resources/gwg-senior-manager.jpg" alt="" class="img-thumbnail">
                </span>
                <p>
                    <span class="profile-header">Jasmin Mohd. Saad</span>
                    <em><small>Governance Working Group &amp; CCT Sr. Manager</small></em>
                </p>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2">
                <span class="my-hover-dark" data-toggle="modal" data-target="#hrd-manager">
                    <img src="http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4372(1).jpg" alt="" class="img-thumbnail img-responsive">
                </span>
                <p>
                    <span class="profile-header">Toni M. Arman</span>
                    <em><small>HRD &amp; Office Management Manager</small></em>
                </p>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2">
                <span class="my-hover-dark" data-toggle="modal" data-target="#communication-manager">
                    <img src="http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4172(1).jpg" alt="" class="img-thumbnail">
                </span>
                <p>
                    <span class="profile-header">Andie Wibianto</span>
                    <em><small>Communication &amp; Information Manager</small></em>
                </p>
            </div>
        </div>
        <!-- end of first row -->
        <div class="row text-center">
            <div class="col-xs-12 col-sm-2 col-md-2">
                <span class="my-hover-dark" data-toggle="modal" data-target="#protocol-manager">
                    <img src="http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4319(1).jpg" alt="" class="img-thumbnail">
                </span>
                <p>
                    <span class="profile-header">Ilham Perintis</span>
                    <em><small>Protocol &amp; Convention Services Manager</small></em>
                </p>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2">
                <span class="" data-toggle="modal" data-target="#finance-asst-manager">
                    <img src="http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4243(1).jpg" alt="" class="img-thumbnail">
                </span>
                <p>
                    <span class="profile-header">Mamimpin Napitupulu</span>
                    <em><small>Finance &amp; Accounting Assistant Manager</small></em>
                </p>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2">
                <span class="" data-toggle="modal" data-target="#legal-asst-manager">
                    <img src="http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4241(1).jpg" alt="" class="img-thumbnail img-responsive">
                </span>
                <p>
                    <span class="profile-header">Arezka Hantyanto</span>
                    <em><small>Legal and Agreement Assistant Manager</small></em>
                </p>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2">
                <span class="my-hover-dark" data-toggle="modal" data-target="#admin-asst-manager">
                    <img src="
    
    http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4271(1).JPG
    " alt="" class="img-thumbnail">
                </span>
                <p>
                    <span class="profile-header">Muhamad Alvin Pahlevi</span>
                    <em><small>Administrative Affairs Assistant Manager</small></em>
                </p>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2">
                <span class="my-hover-dark" data-toggle="modal" data-target="#communication-asst">
                    <img src="http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4273(1).jpg" alt="" class="img-thumbnail">
                </span>
                <p>
                    <span class="profile-header">Kirana Agustina</span>
                    <em><small>Communication &amp; Information Assistant</small></em>
                </p>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2">
                <span class="" data-toggle="modal" data-target="#it-asst">
                    <img src="http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4336(1).jpg" alt="" class="img-thumbnail">
                </span>
                <p>
                    <span class="profile-header">Medy Kesuma Putra</span>
                    <em><small>IT &amp; Software Assistant</small></em>
                </p>
            </div>
        </div>
        <!-- end of second row -->
        <div class="row text-center">
            <div class="col-xs-12 col-sm-2 col-md-2">
                <span class="my-hover-dark" data-toggle="modal" data-target="#technical-asst">
                    <img src="http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4221(1).jpg" alt="" class="img-thumbnail img-responsive">
                </span>
                <p>
                    <span class="profile-header">Destyariani Liana Putri</span>
                    <em><small>Technical Program Assistant</small></em>
                </p>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2">
                <span class="my-hover-dark" data-toggle="modal" data-target="#hrd-asst">
                    <img src="http://coraltriangleinitiative.org/sites/default/files/resources/CTICFF-INCEPTION-day-1-4300(1).jpg" alt="" class="img-thumbnail">
                </span>
                <p>
                    <span class="profile-header">Maria Deswita</span>
                    <em><small>HRD &amp; Office Management Assistant</small></em>
                </p>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2">
                <span class="my-hover-dark" data-toggle="modal" data-target="#it-support">
                    <img src="
	
http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4248(1).jpg
" alt="" class="img-thumbnail">
                </span>
                <p>
                    <span class="profile-header">Windu Margono</span>
                    <em><small>IT Services Support</small></em>
                </p>
            </div>
        </div>
        <!-- end of fourth row -->
    </div>
    <!-- modal -->
    <div id="twg-manager" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- modal-content -->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title my-header">Romina Astrid Lim</h4>
                    <em>Technical Working Group Sr. Manager</em>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 text-left">
                            <img src="http://coraltriangleinitiative.org/sites/default/files/resources/twg-senior-manager_0.jpg" alt="" class="img-thumbnail">
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <p>
                                <i class="fa fa-envelope icon"></i> alim@cticff.org<br>
                                <i class="fa fa-skype icon"></i> astridvlim<br>
                                <i class="fa fa-phone icon"></i> +63 927 8675876</p>
                            <p>
                                Filipino by birth and nationality, Astrid is coordinating the 5 thematic working groups namely: Seascapes, Ecosystem Approach to Fisheries Management (EAFM), Marine Protected Areas (MPA), Climate Change Adaptation (CCA), and Threatened Species (TS).
                            </p>
                            <p>
                                Prior to working with the CTI-CFF Regional Secretariat, she was involved full time for 13 years in environment-related projects in the Philippines which was funded by the United States Agency for International Development - the Coastal Resource Management Project (CRMP) and the Fisheries Improved for Sustainable Project (FISH Project) - as Social Mobilization Specialist and later on as Training and Information Officer. 
                            </p>
                            <p>
                                Astrid was also involved at the tail end of the US CTI Program Integration Project. After that she went back and was involved as part time consultant with the ECOFISH (Ecosystems Improved for Sustainable Fisheries) Project. 
                            </p>
                            <p>
                                She has a Psychology degree from St. Theresa’s College, Cebu. 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of modal bootstrap -->
    <!-- modal -->
    <div id="gwg-manager" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- modal-content -->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title my-header">Jasmin Mohd. Saad</h4>
                    <em>Governance Working Group &amp; CCT Sr. Manager</em>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 text-left">
                            <img src="http://coraltriangleinitiative.org/sites/default/files/resources/gwg-senior-manager.jpg" alt="" class="img-thumbnail">
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <p>
                                <i class="fa fa-envelope icon"></i> jasmin@cticff.org<br>
                                <i class="fa fa-skype icon"></i> jasmin77my<br>
                                <i class="fa fa-phone icon"></i> +6017 296 9226</p>
                            <p>
                                Jasmin is a Malaysian responsible in coordination and communication between the Governance Working Groups (Coordination Mechanism WG; Financial Resources WG; and Monitoring and Evaluation WG) and cross-cutting themes (which includes Women Leaders’ Forum / Youth program; Regional Business Forum; Business Advisory Council; Local Government Network; Scientific Advisory Group; and Capacity Building) with the respective CTI-CFF National Coordinating Committees (NCCs) and Technical Working Groups (TWGs). She is also responsible to seek potential partnerships especially for the cross-cutting themes.
                            </p>
                            <p>
                                Prior to working with the CTI-CFF Regional Secretariat, she was with the Ministry of Science, Technology and Innovation (MOSTI) managing the CTI-CFF national portfolio; she has also worked with WWF Malaysia and Sea Resources Management Sdn Bhd in areas of marine resources management and project management. She was involved in several projects such as the development of the Draft National Oceans Policy; National Integrated Coastal Zone Management Policy; and Legal Aspects of Implementation: Capacity Building for Implementation of the ASEAN Marine Water Quality Criteria. More recent work includes the compilation of resources and report for Asian Development Bank (ADB) on the “State of the Coral Triangle Report” for Malaysia; and publication of the Ecosystem Approach to Fisheries Management: Country Paper (Malaysia)” under the USAID Coral Triangle Support Partnership program. 
                            </p>
                            <p>
                                She has a degree in accounting (B.Acc Hons from Universiti Tenaga Nasional, Malaysia) and post-graduate qualifications in international shipping &amp; logistics; and marine resources management from Plymouth University (United Kingdom) and Australian Maritime College (Australia) respectively.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of modal bootstrap -->
    <!-- modal -->
    <div id="hrd-manager" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- modal-content -->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title my-header">Toni M. Arman</h4>
                    <em>HRD &amp; Office Management Manager</em>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 text-left">
                            <img src="http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4372(1).jpg" alt="" class="img-thumbnail">
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <p>
                                <i class="fa fa-envelope icon"></i> tmarman@cticff.org<br>
                                <i class="fa fa-skype icon"></i> tmap235<br>
                                <i class="fa fa-phone icon"></i> +62816 485 2579</p>
                            <p>
                                Toni is an Indonesia, and he is responsible in the implementation of human resources administrative in Secretariat. He is also responsible for office management in Manado Headquarter.
                            </p>
                            <p>
                                Prior to working with the CTI-CFF Regional Secretariat, he was with several private companies in Indonesia. He worked with PT. Cahaya Buana Group as HRD and General Affairs Division Head, after he left from Suzuki Indonesia. Previously, he joined with Sinar Mas Agribusiness and Technology, Tbk. His extensive experience is in all area of human resources and general affairs with exposure in plantation, manufacturing, and sales distribution multinational companies in Indonesia. 
                            </p>
                            <p>
                                He has a dual bachelor degree in Informatics-Management (from STMIK Gunadarma, Indonesia) and psychology (from University of Indonesia, Indonesia) respectively.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end of modal bootstrap -->
    <!-- modal -->
    <div id="communication-manager" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- modal-content -->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title my-header">Andie Wibianto</h4>
                    <em>Communication &amp; Information Manager</em>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 text-left">
                            <img src="http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4172(1).jpg" alt="" class="img-thumbnail">
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <p>
                                <i class="fa fa-envelope icon"></i> andiewibi@cticff.org<br>
                                <i class="fa fa-skype icon"></i> andie.wibi<br>
                                <i class="fa fa-phone icon"></i> +62856 765 3939</p>
                            <p>
                                Andie is responsible in managing, coordinating, and distributing information and communication to both internal and external stakeholders.  
                            </p>
                            <p>
                                Before joining the CTI-CFF Regional Secretariat, he was working with USAID Indonesia-WWF USA project of MPAG (Marine Protected Area Governance) in collaboration with a consortium of prominent marine NGOs and Ministry of Marine Affairs and Fisheries (MMAF) of Indonesia.  He was also a seasoned Public Relations practitioner in managing various national and international blue chip corporations’ clientele for more than 10 years.  In addition to that, as a certified Instructional Designer (ID) of Electronic Learning/E-Learning, he had succeeded in developing more than 100 E-Learning hours of various module courses for several Indonesian reputable corporations.
                            </p>
                            <p>
                                He earns a bachelor degree in Mass Communication and Journalism and an English diploma with focuses on advertising and tourism from University of Moestopo (B), Jakarta and University of Indonesia, Depok respectively.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end of modal bootstrap -->
    <!-- modal -->
    <div id="protocol-manager" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- modal-content -->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title my-header">Ilham Perintis</h4>
                    <em>Protocol &amp; Convention Services Manager</em>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 text-left">
                            <img src="http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4319(1).jpg" alt="" class="img-thumbnail">
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <p>
                                <i class="fa fa-envelope icon"></i> iperintis@cticff.org<br>
                                <i class="fa fa-skype icon"></i> ilham.perintis<br>
                                <i class="fa fa-phone icon"></i> +62811 233 0705
                            </p>
                            <p>
                                Mr. Ilham Perintis is responsible to facilitate, manage and coordinate Secretarial meetings and international events of principal organs, such as the Meetings of Ministerial and Senior Officials' levels. He is also responsible to overview protocol and standard operating procedures for organizational and convention matters. Additionally, He assists Finance and Administration Senior Manager in preparing draft annual budget and financial regulations (i.e. rules of procedures) as well as corporate policies. In regular basis, he prepares administrative substances (e.g. letters, speeches, reports, agendas, correspondences, schedule matters, etc.). 
                            </p>
                            <p>
                                Mr. Perintis worked at the Secretariat of D-8 Organization for Economic Cooperation for seven years as Executive Assistant Director. He had extensive engagement with the political and economic matters in the regional and sub-regional contexts. He also had responsible in overseeing the development of key programmes of economic and areas of cooperation (i.e. agriculture, trade, industry &amp; SMEs, transportation and energy). He also had vast exposure in monetary, financial and banking researches and prepared position papers for high-end users on economic development during his time as Researcher at Central Bank of Indonesia.  
                            </p>
                            <p>
                                Mr. Perintis holds B.Sc. in International Economics from Faculty of Economics and Business, University of Indonesia. His interests include, among others, economic development, national competitiveness, monetary, trade, business environment and globalization. He is married with two children. 
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end of modal bootstrap -->
    <!-- modal -->
    <div id="admin-asst-manager" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- modal-content -->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title my-header">Muhamad Alvin Pahlevi</h4>
                    <em>Administrative Affairs Assistant Manager</em>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 text-left">
                            <img src="
	
http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4271(1).JPG
" alt="" class="img-thumbnail">
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <p>
                                <i class="fa fa-envelope icon"></i> apahlevi@cticff.org<br>
                                <i class="fa fa-skype icon"></i> alvinpahlevi<br>
                                <i class="fa fa-phone icon"></i> -
                            </p>
                            <p>
                                Alvin Pahlevi is Indonesian working as Administrative Assistant Manager for CTI-CFF Regional Secretariat. He joined CTI-CFF Regional Secretariat in 2015 as administrative assistant where he was seconded  by Ministry of Marine Affairs and Fisheries before taking on his current role in 2016 in the CTI-CFF Regional Secretariat Headquarter where he now manages the administrative support functions of the correspondence between the Regional Secretariat with stakeholders of CTI-CFF and also to supports the management for the administrative of correspondence system.
                            </p>
                            <p>
                                Alvin has a Bachelor's Degree in International Relations from Fatih University, Turkey and a Master's Degree in International Relations (major in Global Security and Development), from the University of Plymouth.  
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end of modal bootstrap -->
    <!-- modal -->
    <div id="communication-asst" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- modal-content -->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title my-header">Kirana Agustina</h4>
                    <em>Communication &amp; Information Assistant</em>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 text-left">
                            <img src="http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4273(1).jpg" alt="" class="img-thumbnail">
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <p>
                                <i class="fa fa-envelope icon"></i> k.agustina@cticff.org<br>
                                <i class="fa fa-skype icon"></i> Kirana.agustina<br>
                                <i class="fa fa-phone icon"></i> +62812 234 41188
                            </p>
                            <p>
                                Through her role, as Communication Outreach and National Coordinating Committee (NCC) Point of Contact, Kirana is responsible for advising the Regional Secretariat coordinator on communications and outreach options and serve as the key point of contact for day-to-day communications with NCC, as well as media.
                            </p>
                            <p>
                                The work with CTI-CFF greatly complements the understandings that she have gained through working for the government of Indonesia as a consultant at the Ministry of Marine Affair and Fisheries, where she was in charged of developing sustainable alternative livelihoods in coastal fishing communities in East Borneo under Japan for Poverty Reduction (JFPR) and ADB-RETA project. And previously, She worked for a local NGO, Friends of the National Parks Foundation, as a Communications Manager. The NGO focused on wildlife conservation, reforestation combined with community development at the grass root level. Being based in the field, She gained first hand experience in project implementation and dynamics of conservation on the ground, as well as connections with small local NGOs across Indonesia. 
                            </p>
                            <p>
                                Kirana, who joined Regional Secretariat CTI-CFF in October 2015, graduated from Indonesia’s University of Padjadjaran with a Bachelor in Marine Science in 2012.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end of modal bootstrap -->
    <!-- modal -->
    <div id="technical-asst" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- modal-content -->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title my-header">Destyariani Liana Putri</h4>
                    <em>Technical Program Assistant</em>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 text-left">
                            <img src="http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4221(1).jpg" alt="" class="img-thumbnail">
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <p>
                                <i class="fa fa-envelope icon"></i> dlputri@cticff.org<br>
                                <i class="fa fa-skype icon"></i> destyariani.putri <br>
                                <i class="fa fa-phone icon"></i> +62856 4931 2537
                            </p>
                            <p>
                                Putri is an Indonesian responsible in any technicalities support of Technical Program Division.  She is also closely work with Technical Working Group and Cross Cutting Themes towards the establishment of CTI-CFF RPOA within National Coordination Committees (NCCs), Partners, Collaboration and University Partnership as well. Along with the commitment of Technical Program Division, her work ethics have been stimulated as a system within others division in the CTI-CFF Regional Secretariat. 
                            </p>
                            <p>
                                She is a fresh graduated master student who also experienced in the national research program, scientific journal writing and national company prior to working with the CTI-CFF Regional Secretariat. As the young researcher, she had involved in research for district innovation system in the East Java, Indonesia and share the output into scientific writing which can be found in Science Direct. Besides her writing ability, she has been getting work with the public-private company for environmental analysis in particular on the coastal processes.
                            </p>
                            <p>
                                She has a degree in ocean engineering (Bachelor of Engineering from Institut Teknologi Sepuluh Nopember, Indonesia) and her master for coastal engineering and management (Master of Engineering from Institut Teknologi Sepuluh Nopember, Indonesia).
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end of modal bootstrap -->
    <!-- modal -->
    <div id="hrd-asst" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- modal-content -->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title my-header">Maria Deswita</h4>
                    <em>HRD &amp; Office Management Assistant</em>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 text-left">
                            <img src="http://coraltriangleinitiative.org/sites/default/files/resources/CTICFF-INCEPTION-day-1-4300(1).jpg" alt="" class="img-thumbnail">
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <p>
                                <i class="fa fa-envelope icon"></i> mdeswita@cticff.org<br>
                                <i class="fa fa-skype icon"></i> mariadeswita13<br>
                                <i class="fa fa-phone icon"></i> +62812 3572 9242
                            </p>
                            <p>
                                Ms. Maria Deswita is responsible on personnel and office management administrative matters. Additionally, she also supports logistic and administration on Secretarial Meetings and International Events. 
                            </p>
                            <p>
                                Ms. Deswita worked at the Wildlife Conservation Society (WCS) for six months as Consultant. As consultant of WCS, she did Socio-Economic Survey for Conservation Area Preparation in Sumbawa Island and for Fisheries Business Chain in Lesser Sunda Island project. Previously, she was a Research Assistant for six months in MEXMA Research Group, Brawijaya University.
                            </p>
                            <p>
                                Ms. Deswita holds B.Sc. in Marine Science from Faculty of Fisheries and Marine Science, Brawijaya University, Indonesia. Her interests include, among others, marine conservation, marine biotechnology, and microbiology.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end of modal bootstrap -->
    <!-- modal -->
    <div id="it-support" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- modal-content -->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title my-header">Windu Margono</h4>
                    <em>IT Services Support</em>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 text-left">
                            <img src="
	
http://coraltriangleinitiative.org/sites/default/files/resources/STAFFCTI-4248(1).jpg
" alt="" class="img-thumbnail">
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <p>
                                <i class="fa fa-envelope icon"></i> wmargono@cticff.org<br>
                                <i class="fa fa-skype icon"></i> winlimbong<br>
                                <i class="fa fa-phone icon"></i> +62878 8228 8415
                            </p>
                            <p>
                                Windu is a Indonesian, and he Performs a variety of moderately complex information technology support duties to ensure smooth delivery of technology services. Monitors, operates, or coordinates and assists others in the operation of computer hardware, software, and peripherals in order to achieve desired results.
                            </p>
                            <p>
                                Prior to working with the CTI-CFF Regional Secretariat, he worked in several private business sectors in Jakarta, even opening his own shop once he did so. He works in Multimedia Solution Store as a Software &amp; Hardware installation technician and also marketing. Previously he joined a joint venture called Download Center that focuses on the gadget sector, such as game installations and applications.
                            </p>
                            <p>
                                He studied at a vocational-based school called Riset dan Teknologi Jaya Jakarta, with a major in Automotive, having an internship experience at Mitsubishi Kramayudha Motor and Toyota Motor Manufacturing Indonesia.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end of modal bootstrap -->
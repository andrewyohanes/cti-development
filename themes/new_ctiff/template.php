<?php
/**
 * Rewrite the terms links to custom view
 */

function display_cea_terms($node, $vid = NULL, $ordered_list = TRUE) {
     
      //Count the number of taxonomy for each node
      $tax_data=$node->taxonomy;     
      $total_tax=count($tax_data);
     
      $vocabularies = taxonomy_get_vocabularies();
      $iv=0;
      $tot_v=count($vocabularies);
       
      //if ($ordered_list) $output .= '<ul>'; //checks to see if you want an ordered list
      if ($vid) { //checks to see if you've passed a number with vid, prints just that vid
        $output = '<div class="tags-'. $vid . '">';
        foreach($vocabularies as $vocabulary) {
         if ($vocabulary->vid == $vid) {
           $terms = taxonomy_node_get_terms_by_vocabulary($node, $vocabulary->vid);
           if ($terms) {
             $links = array();
             $output .= '<span class="only-vocabulary-'. $vocabulary->vid . '">';
             if ($ordered_list) $output .= '<li class="vocabulary-'. $vocabulary->vid . '">';
             
             foreach ($terms as $term) {
               if($vocabulary->name=='Resource Type'){
                    $term_path='resource_by_type/'.$term->tid;
               }
               else if($vocabulary->name=='Primary Subject'){
                    $term_path='resource_by_subject/'.$term->tid;
               }
	       else if($vocabulary->name=='Secondary Subject'){
                    $term_path='resource_by_subject/'.$term->tid;
               }
               else if($vocabulary->name=='Country'){
                    $term_path='resource_by_country/'.$term->tid;
               }
               else if($vocabulary->name=='Events'){
                    $term_path='cea_event_type/'.$term->tid;
               }
		if($iv<$total_tax-1){
		  $links[] = '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description))) .' | </ li>';
		}
		else{
		  $links[] = '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description))) .'</ li>';
		}
		$iv++;             }
	      $output .= implode('  ', $links);
	      if ($ordered_list) $output .= '</li>';
	      $output .= '</span>';
	    }
	  }
	}
      }
      else {
       //$output = '<div class="taxonomy">';
       $term_psubject1=array();
       foreach($vocabularies as $vocabulary) {
         if ($vocabularies) {
           $terms = taxonomy_node_get_terms_by_vocabulary($node, $vocabulary->vid);
           if ($terms) {
             $links = array();
             if ($ordered_list) //$output .= '<li class="vocabulary-'. $vocabulary->vid . '">';
	     
             foreach ($terms as $k=>$term) {
	       
              if($vocabulary->name=='Type'){
                    $term_path='resource_by_type/'.$term->tid;
		    $term_type .= '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description)));
		    if(count($terms)!=$k+1)
		    {
		      $term_type.=' | ';
		    }
		     $term_type.='</li>';	
               }
			   
                if($vocabulary->name=='Subject'){
                    $term_path='resource_by_subject/'.$term->tid;
		    $term_psubject1[] = '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description)));

		     	    
               }
	      /* else if($vocabulary->name=='Secondary Subject'){
                    $term_path='resource_by_subject/'.$term->tid;
		    $term_psubject1[] = '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description)));
    
               }*/
               else if($vocabulary->name=='Country'){
                    $term_path='resource_by_country/'.$term->tid;
		    $term_country .= '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description)));
		    if(count($terms)!=$k+1)
		    {
		      $term_country.=' | ';
		    }
		     $term_country.='</li>';		    
               }
	       /*else if($vocabulary->name=='ECO Asia Event'){
                    $term_path='cea_event_type/'.$term->tid;
		    $term_events .= '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description)));
		    if(count($terms)!=$k+1)
		    {
		      $term_events.=' | ';
		    }
		     $term_events.='</li>';		    
               } */
             }
	      
             //$output .= implode('  ', $links);
	  }
	  
	}
	
      }

      if(count($term_psubject1)==1){
	$term_psubject=$term_psubject1[0].'</ li>';
      }
      elseif(count($term_psubject1)>1){
	$term_psubject=implode(' | </ li>',$term_psubject1).'</ li>';
      }
      
      
    }
      if($term_psubject){
       $term_subject_link ='<div class="term_format_link"><ul><li class="voc_name">Subject: </ li>'.$term_psubject.'</ul><div style="clear:both"></div></div>';
      }
      if($term_type){
       $term_type_link='<div class="term_format_link"><ul><li class="voc_name">Type: </ li>'.substr_replace($term_type,"",-7).'</ul><div style="clear:both"></div></div>';
      }
      if($term_country){
       $term_country_link='<div class="term_format_link"><ul><li class="voc_name">Country: </ li>'.substr_replace($term_country,"",-7).'</ul><div style="clear:both"></div></div>';
      }
     // if($term_events){
      // $term_event_link='<div class="term_format_link"><ul><li class="voc_name">Event: </ li>'.substr_replace($term_events,"",-7).'</ul><div style="clear:both"></div></div>';
     // }
      
      $output .=$term_subject_link.$term_type_link.$term_country_link.$term_event_link;
      
      return $output;
}
  
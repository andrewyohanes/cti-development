<?php
  // $Id: views-view-list.tpl.php,v 1.3 2008/09/30 19:47:11 merlinofchaos Exp $
  /**
   * @file views-view-list.tpl.php
   * Default simple view template to display a list of rows.
   *
   * - $title : The title of this group of rows.  May be empty.
   * - $options['type'] will either be ul or ol.
   * @ingroup views_templates
   *
   * 
   */
  
  $viewData = $view->result;
  $title = $view->display_handler->options['title'];
?>
<style>
  section.fullwidth-page .entry-header h1{
    display: none;
  }
</style>
<section>
  <div class="entry-header">
    <h2>
      Resources By Type
    </h2>
    <hr>
  </div>
  <div class="entry-content">
    <div class="item-list">
      <?php if (!empty($title)) : ?>
        <h3><?php print $title; ?></h3>
      <?php endif; ?>
  
      <<?php print $options['type']; ?>>
        <?php foreach($viewData AS $viewValue){
          $urlValue = $base_path."resource_by_type/".$viewValue->tid;
          $termName = $viewValue->term_data_name;
        ?>
      
          <li class="<?php print $classes[$id]; ?>"><?php print $titleValue = l($termName, $urlValue);; ?></li>
        <?php
          }
        ?>
      </<?php print $options['type']; ?>>  
    </div>
  </div>
</section>

<?php
/**
 * @file views-view-grid.tpl.php
 * Default simple view template to display a rows in a grid.
 *
 * - $rows contains a nested array of rows. Each row contains an array of
 *   columns.
 * - $class contains the class of the table.
 * - $attributes contains other attributes for the table.
 * @ingroup views_templates
 */

global $base_url;
global $base_path;

$breadcrumb = array();
$breadcrumb[] = l('Home', '');
$breadcrumb[] = l(drupal_get_title(), $base_url . base_path() . $view->display_handler->options['path']);

// Set Breadcrumbs
$breadcrumbs = drupal_set_breadcrumb($breadcrumb);
$title = $view->display_handler->options['title'];
?>
<div class="views-grid">
  <div class="entry-header">
    <div class="content-header">
      <div class="breadcrumb">
        <ol class="breadcrumb">
          <?php foreach($breadcrumbs as $breadcrumb): ?>
            <li><?php echo $breadcrumb; ?></li>
          <?php endforeach; ?>
        </ol>
      </div>
      <?php if (!empty($title)) : ?>
        <h1><?php print $title; ?></h1>
      <?php endif; ?>
    </div>
  </div>
  <div class="container">
    <div class="view-grid-wrap <?php print $class; ?>" <?php print $attributes; ?>>
      <?php foreach ($rows as $row_number => $columns): ?>
        <?php foreach ($columns as $column_number => $item): ?>
          <?php if(!empty($item)): ?>
            <div class="view-grid-item  <?php print $column_classes[$row_number][$column_number]; ?>">
              <div class="border-content">
                <?php print $item; ?>
              </div>
            </div>
          <?php endif; ?>
        <?php endforeach; ?>
      <?php endforeach; ?>
    </div>
  </div>
</div>
<div class="wrap-resourcemap">
<?php
  $rootheme = check_url($front_page) .'sites/all/themes/new_ctiff';
?>
    <style>
      path {
        cursor: pointer;
      }
    </style>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/d3/3.5.3/d3.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/topojson/1.6.9/topojson.min.js"></script>
    <script src="<?php print $rootheme; ?>/js/datamaps.all.min.js"></script>
    <div style="background-color:#f8f8f8; width: 100%;">
    <div class="container">
      <section>
        <div class="entry-header">
          <h2>
            Resources By Country
          </h2>
          <a href="<?php print $front_page; ?>/cti-cff-regional-map">
            <i class="fa fa-th"></i>
            See All
          </a>
          <hr>
        </div>
      </section>
      <div id="container" style="position: relative; width: 100%; height: auto;"></div>
    </div>
    </div>
    <script>
      var map = new Datamap({
        element: document.getElementById("container"),
        scope: 'world',
        setProjection: function(element) {
          var projection = d3.geo.equirectangular()
            .center([140, 0])
            .rotate([4.4, 0])
            .scale(550)
            .translate([element.offsetWidth / 2, element.offsetHeight / 2]);
          var path = d3.geo.path()
            .projection(projection);

          return {path: path, projection: projection};
        },
        fills: {
          defaultFill: "#ABDDA4",
          gt50: "#663399",
        },
        data: {
          'IDN': { fillKey: 'gt50' },
          'PHL': { fillKey: 'gt50' },
          'PNG': { fillKey: 'gt50' },
          'SLB': { fillKey: 'gt50' },
          'MYS': { fillKey: 'gt50' },
          'TLS': { fillKey: 'gt50' },
        },
        responsive: true,
        done: function(datamap) {
            datamap.svg.selectAll('.datamaps-subunit').on('click', function(geography) {
              switch(geography.id){
                case "IDN":
                  location.href="<?php print $front_page;?>/resource_by_country/71";
                  break;
                case "PHL":
                  location.href="<?php print $front_page;?>/resource_by_country/72";
                  break;
                case "PNG":
                  location.href="<?php print $front_page;?>/resource_by_country/108";
                  break;
                case "SLB":
                  location.href="<?php print $front_page;?>/resource_by_country/109";
                  break;
                case "MYS":
                  location.href="<?php print $front_page;?>/resource_by_country/107";
                  break;
                case "TLS":
                  location.href="<?php print $front_page;?>/resource_by_country/110";
                  break;
              }
            });
        }
      });
      shape = document.getElementsByTagName("svg")[0];
      var screenWidth =  $(window).width();
      if(screenWidth<=576){
        shape.setAttribute("viewBox", "-250 -150 900 450"); 
      }else if(screenWidth<=767){
         shape.setAttribute("viewBox", "-150 -100 900 450"); 
      }else{
        shape.setAttribute("viewBox", "-50 0 900 450"); 
      }
      shape.setAttribute("width", "900"); 
      shape.setAttribute("height", "450"); 
      shape.setAttribute("data-width", "900"); 
    </script>
    </div>